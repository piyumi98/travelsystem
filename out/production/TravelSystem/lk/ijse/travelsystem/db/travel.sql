drop database travel ;
create database travel;
use travel;

create table traveler(
                       travelID varchar(255) not null ,
                       travelerFullName varchar(255) not NULL unique ,
                       travelerCountry varchar(255) not NULL ,
                       travelerMobileNumber varchar(255) not null,
                       travelerEmail varchar(255) not null,
                       constraint primary key (travelID)
);

create table payment(
                      paymentID varchar(255) not null,
                      paymentAmount double(10,2) not null,
                      paymentDate date not null ,
                      constraint primary key (paymentID)

);

create table guide(
                    guideID int(11) not null auto_increment,
                    guideName varchar(255) not null ,
                    guideAddress varchar(255) not null ,
                    guideTelNo varchar(255) not  null,
                    constraint primary key (guideID)
);

create table langauges(
                        langName varchar(255) not null ,
                        constraint primary key (langName)
);

insert into langauges values ('CHINESE');
insert into langauges values ('SPANISH');
insert into langauges values ('ENGLISH');
insert into langauges values ('HINDI');
insert into langauges values ('ARABIC');
insert into langauges values ('PORTUGUESE');
insert into langauges values ('BENGALI');
insert into langauges values ('RUSSIAN');
insert into langauges values ('JAVANESE');
insert into langauges values ('GERMAN');
insert into langauges values ('KOREAN');
insert into langauges values ('FRENCH');
insert into langauges values ('TURKISH');
insert into langauges values ('SINHALA');

create table guideDetails(
                           guideID int(11) not null,
                           langName varchar(255) not null ,
                           constraint primary key (guideID,langName),
                           constraint foreign key(guideID) references guide(guideID)
                             ON UPDATE CASCADE ON DELETE CASCADE,
                           constraint foreign key(langName) references langauges(langName)
                             ON UPDATE CASCADE ON DELETE CASCADE
);

create table package(
                      packageName varchar(255) not null,
                      guideID int(11) not null,
                      tourBeginDate date not null,
                      tourEndDate date not null,
                      tourDuration int not null,
                      packagePrice decimal(10,2) not NULL ,
                      constraint primary key (packageName),
                      constraint foreign key(guideID) references guide(guideID)
                        ON UPDATE CASCADE ON DELETE CASCADE
);

create table booking(
                      bookingID varchar(255) not null,
                      travelID varchar(255) not null ,
                      paymentid varchar(255) not null ,
                      packageName varchar(255) not null,
                      bookingDate date not null ,
                      bookingPersonCount int not null,
                      constraint primary key (bookingID),
                      constraint foreign key(travelID) references traveler(travelID)
                        ON UPDATE CASCADE ON DELETE CASCADE,
                      constraint foreign key(paymentid) references payment(paymentid)
                        ON UPDATE CASCADE ON DELETE CASCADE,
                      constraint foreign key(packageName) references package(packageName)
                        ON UPDATE CASCADE ON DELETE CASCADE
);

create table mainlocation(
                           mainLocationName varchar(255) not null ,
                           locationDescption varchar(255) not null ,
                           locationDIstance decimal(10,2) not null,
                           avgTimeTOGOhours int(11) not null,
                           constraint primary key (mainLocationName)
);

insert into mainlocation values ('Mirissa Beach','Mirissa',180.0,5);
insert into mainlocation values ('Rathnapura',' Adam''s Peak',200.0,6);
insert into mainlocation values ('Kandy','Kandy and the Temple of the Tooth',160.0,5);
insert into mainlocation values ('Katharagama',' Yala National Park',250.0,7);
insert into mainlocation values ('Pottuvil','Arugam Bay',345.0,8);
insert into mainlocation values ('Galle','Galle and his Dutch Fort',120.0,3);
insert into mainlocation values ('Nuwara Eliya','Nuwara Eliya and Tea Plantation',240.0,6);
insert into mainlocation values ('Polonnaruwa','Kings ruled the central plains of Sri Lanka from Polonnaruwa 800 years ago',340.0,7);
insert into mainlocation values ('Dambulla','Dambulla Golden Cave Temple',390.0,7);

create table sublocation(
                          sublocationName varchar(255) not null ,
                          locationDIstance decimal(10,2) not null,
                          constraint primary key (sublocationName)
);

create table locationDetail(
                             mainLocationName varchar(255) not null ,
                             sublocationName varchar(255) not null ,
                             constraint primary key (sublocationName,mainLocationName),
                             constraint foreign key(mainLocationName) references mainlocation(mainLocationName)
                               ON UPDATE CASCADE ON DELETE CASCADE,
                             constraint foreign key(sublocationName) references sublocation(sublocationName)
                               ON UPDATE CASCADE ON DELETE CASCADE
);

create table hotel(
                    hotelID int(11) not null auto_increment,
                    mainLocationName varchar(255) not null ,
                    hotelName varchar(255) not null ,
                    hotelAddress varchar(255) not null ,
                    hotelLocation varchar(255) not null ,
                    hotelTel varchar(255) not null ,
                    constraint primary key (hotelID),
                    constraint foreign key(mainLocationName) references mainlocation(mainLocationName)
                      ON UPDATE CASCADE ON DELETE CASCADE
);

create table hoteldetail(
                          hotelID int(11) not null ,
                          packageName varchar(255) not null,
                          constraint primary key (hotelID,packageName),
                          constraint foreign key(hotelID) references hotel(hotelID)
                            ON UPDATE CASCADE ON DELETE CASCADE,
                          constraint foreign key(packageName) references package(packageName)
                            ON UPDATE CASCADE ON DELETE CASCADE
);


create table user(
                    userID int(11) not null auto_increment,
                     username varchar(255) not null ,
                     password varchar(255) not null ,
                     soltvalue varchar(255) not null,

                     constraint primary key (userID)
);

