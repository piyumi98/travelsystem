package lk.ijse.travelsystem.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import lk.ijse.travelsystem.bo.custom.GuideBO;
import lk.ijse.travelsystem.bo.custom.PackageBO;
import lk.ijse.travelsystem.bo.custom.impl.GuideBOImpl;
import lk.ijse.travelsystem.bo.custom.impl.PackageBOImpl;
import lk.ijse.travelsystem.db.DBConnection;
import lk.ijse.travelsystem.model.GuideDTO;
import lk.ijse.travelsystem.model.PackageDTO;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;


import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ResourceBundle;

public class DashboardViewController implements Initializable {

    @FXML
    private TableView<GuideDTO> tblGuide;

    @FXML
    private TableView<PackageDTO> tblPackage;

    private static GuideBO guideBO=new GuideBOImpl();
    private static PackageBO packageBO=new PackageBOImpl();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        tblGuide.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("guideID"));
        tblGuide.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("guideName"));
        tblGuide.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("guideAddress"));
        tblGuide.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("guideTelNo"));

        tblPackage.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("packageName"));
        tblPackage.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("guideID"));
        tblPackage.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("tourBeginDate"));
        tblPackage.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("tourEndDate"));
        tblPackage.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("tourDuration"));
        tblPackage.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("packagePrice"));

        try {
            loadGuides();
            loadPackages();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadPackages() throws Exception {
        tblPackage.setItems(FXCollections.observableArrayList(packageBO.getAllPackage()));
    }



    private void loadGuides() throws Exception {
        tblGuide.setItems(FXCollections.observableArrayList(guideBO.getAllGuides()));
    }

    @FXML
    void onLoadTravelersReport(MouseEvent event) throws SQLException, ClassNotFoundException, JRException {
        InputStream inputStream=getClass().getResourceAsStream("/lk/ijse/travelsystem/report/Best_Customers.jasper");
        HashMap hashMap=new HashMap();

        JasperPrint jasperPrint= JasperFillManager.fillReport(inputStream,hashMap, DBConnection.getInstance().getConnection());
        JasperViewer.viewReport(jasperPrint,false);
    }

    @FXML
    void onLoadGuideReports(MouseEvent event) throws SQLException, ClassNotFoundException, JRException {
        InputStream inputStream=getClass().getResourceAsStream("/lk/ijse/travelsystem/report/BestGuide.jasper");
        HashMap hashMap=new HashMap();

        JasperPrint jasperPrint= JasperFillManager.fillReport(inputStream,hashMap, DBConnection.getInstance().getConnection());
        JasperViewer.viewReport(jasperPrint,false);
    }

    @FXML
    void onLoadPackagesPayments(MouseEvent event) throws SQLException, ClassNotFoundException, JRException {
        InputStream inputStream=getClass().getResourceAsStream("/lk/ijse/travelsystem/report/paymentsForPackages.jasper");
        HashMap hashMap=new HashMap();

        JasperPrint jasperPrint= JasperFillManager.fillReport(inputStream,hashMap, DBConnection.getInstance().getConnection());
        JasperViewer.viewReport(jasperPrint,false);
    }

    @FXML
    void onLoadTravelersLikePackages(MouseEvent event) throws SQLException, ClassNotFoundException, JRException {
        InputStream inputStream=getClass().getResourceAsStream("/lk/ijse/travelsystem/report/PersonLikePackage.jasper");
        HashMap hashMap=new HashMap();

        JasperPrint jasperPrint= JasperFillManager.fillReport(inputStream,hashMap, DBConnection.getInstance().getConnection());
        JasperViewer.viewReport(jasperPrint,false);
    }

}
