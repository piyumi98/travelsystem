package lk.ijse.travelsystem.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import lk.ijse.travelsystem.common.PasswordUtill;
import lk.ijse.travelsystem.dao.CrudUtil;

import java.io.IOException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.Optional;

public class NewUserController {

    @FXML
    private AnchorPane root;

    @FXML
    private JFXTextField txtuserName;

    @FXML
    private JFXPasswordField txtpassword;

    @FXML
    private JFXPasswordField txtconform;

    @FXML
    private JFXButton btnadd;

    @FXML
    void onAdd(ActionEvent event) {

        try {
            String uname = txtuserName.getText();
            String pw = txtpassword.getText();
            String salt = PasswordUtill.getSalt(30);
            String secPw=PasswordUtill.generateSecurePassword(pw, salt);
            boolean b = CrudUtil.executeUpdate("Insert into user values(?,?,?,?)", 0, uname, salt, secPw);
            if (b){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Your Sign up is Success..",ButtonType.OK);
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    Parent parentRoot = FXMLLoader.load(this.getClass().getResource("/lk/ijse/travelsystem/view/loginView.fxml"));
                    Scene scene = new Scene(parentRoot);

                    Stage window = (Stage) this.root.getScene().getWindow();
                    window.setScene(scene);
                    window.centerOnScreen();

//                    TranslateTransition tran = new TranslateTransition(Duration.millis(1000), scene.getRoot());
//                    tran.setToX(0);
//                    tran.setFromX(-scene.getWidth());
//                    tran.play();
                }
            }else {
                Parent parentRoot = FXMLLoader.load(this.getClass().getResource("/lk/ijse/travelsystem/view/newUser.fxml"));
                Scene scene = new Scene(parentRoot);

                Stage window = (Stage) this.root.getScene().getWindow();
                window.setScene(scene);
                window.centerOnScreen();
            }
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
