package lk.ijse.travelsystem.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import lk.ijse.travelsystem.bo.BOFactory;
import lk.ijse.travelsystem.bo.custom.BookingBO;
import lk.ijse.travelsystem.bo.custom.PackageBO;
import lk.ijse.travelsystem.bo.custom.PaymentBO;
import lk.ijse.travelsystem.bo.custom.TravelerBO;
import lk.ijse.travelsystem.bo.custom.impl.BookingBOImpl;
import lk.ijse.travelsystem.bo.custom.impl.PackageBOImpl;
import lk.ijse.travelsystem.bo.custom.impl.PaymentBOImpl;
import lk.ijse.travelsystem.common.IDGenarator;
import lk.ijse.travelsystem.common.NotificationController;
import lk.ijse.travelsystem.common.Validation;
import lk.ijse.travelsystem.model.*;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class TravelerViewController implements Initializable {
    @FXML
    private JFXTextField txtTravelID;

    @FXML
    private JFXTextField txtTravelName;

    @FXML
    private JFXTextField txtCoutry;

    @FXML
    private JFXTextField txtTraavelerMobi;

    @FXML
    private JFXTextField travelerEmail;

    @FXML
    private JFXButton btnAdd;

    @FXML
    private JFXTextField txtTourBeginDate;

    @FXML
    private JFXTextField txtTourEndDate;

    @FXML
    private JFXTextField txtTourDuration;

    @FXML
    private JFXTextField txtPackagePrice;

    @FXML
    private JFXComboBox<String> comboSelectPackage;

    @FXML
    private JFXTextField txtSearchTravelerName;

    @FXML
    private JFXButton btnSearch;

    @FXML
    private JFXTextField txtSearcgTravelerID;

    @FXML
    private JFXTextField txtBookingID;

    @FXML
    private JFXTextField txtPaymentID;

    @FXML
    private JFXTextField txtBookingPersonsCount;

    @FXML
    private JFXTextField txtPayment;

    @FXML
    private TableView<BookingDTO> tblBooking;

    @FXML
    private JFXButton btnBooking;

    @FXML
    private JFXButton btnTravelerDel;

    @FXML
    private JFXButton btntrvelerUpdate;

    @FXML
    private JFXButton btnUpdateBooking;

    @FXML
    private TableView<TravelerDTO> tblTravelTbl;

    @FXML
    private TextField datePickBookingDate;




    private static TravelerBO travelerBO;
    private static PackageBO packageBO=new PackageBOImpl();
    private static BookingBO bookingBO=new BookingBOImpl();
    private static PaymentBO paymentBO=new PaymentBOImpl();

    public TravelerViewController() {
        travelerBO= (TravelerBO) BOFactory.getInstance().getBO(BOFactory.BOTyepes.TRAVELER);
    }

    public void onActionAdd(ActionEvent actionEvent) throws Exception {
        if (ValidationTraveler()){
            TravelerDTO travelerDTO=new TravelerDTO(
                    txtTravelID.getText(),
                    txtTravelName.getText(),
                    txtCoutry.getText(),
                    txtTraavelerMobi.getText(),
                    travelerEmail.getText()
            );
            boolean result = travelerBO.addTraveler(travelerDTO);
            if (result){
                clearAll();
                callAll();
                NotificationController.createSuccesful("Added Successfully", " Traveler Added Successfully");
            }else{
                NotificationController.createError("Added not Successfully", " Added not Successfully");
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            timeAndDate();
            txtBookingPersonsCount.setStyle("-fx-background-color: #8B8E8F    ");
            callAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callAll() throws Exception {
        loadBookingTable();
        loadPackages();
        setNewPID();
        setNewTravelerID();
        setNewBookingID();
        loadTravelerTable();
    }

    private void loadBookingTable() throws Exception {
        tblBooking.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("bookingID"));
        tblBooking.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("travelID"));
        tblBooking.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("paymentid"));
        tblBooking.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("packageName"));
        tblBooking.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("bookingDate"));
        tblBooking.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("bookingPersonCount"));

        tblBooking.setItems(FXCollections.observableArrayList(bookingBO.getAllBookings()));
    }

    private void loadTravelerTable() throws Exception {
        tblTravelTbl.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("travelID"));
        tblTravelTbl.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("travelerFullName"));
        tblTravelTbl.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("travelerCountry"));
        tblTravelTbl.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("travelerMobileNumber"));
        tblTravelTbl.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("travelerEmail"));

        tblTravelTbl.setItems(FXCollections.observableArrayList(travelerBO.getAllTravelers()));
    }

    @FXML
    void travelTableClick(MouseEvent event) {
        TravelerDTO travelerDTO=tblTravelTbl.getSelectionModel().getSelectedItem();
        txtTravelID.setText(travelerDTO.getTravelID());
        txtTravelName.setText(travelerDTO.getTravelerFullName());
        txtCoutry.setText(travelerDTO.getTravelerCountry());
        txtTraavelerMobi.setText(travelerDTO.getTravelerMobileNumber());
        travelerEmail.setText(travelerDTO.getTravelerEmail());
    }

    private void setNewTravelerID() throws Exception {
        String chid;
        chid= IDGenarator.getNewID("traveler","travelID","T");
        txtTravelID.setText(chid);
    }

    private void setNewBookingID() throws Exception {
        String chid;
        chid= IDGenarator.getNewID("booking","bookingID","B");
        txtBookingID.setText(chid);
    }

    private void loadPackages() throws Exception {
        ArrayList<PackageDTO> levelIDS=packageBO.getAllPackage();
        ArrayList<String>ids=new ArrayList<>();
        for (PackageDTO dto:levelIDS) {
            ids.add(dto.getPackageName());
        }
        comboSelectPackage.getItems().addAll(ids);

    }

    @FXML
    void loadDetails(ActionEvent event) throws Exception {
        String packageName=comboSelectPackage.getSelectionModel().getSelectedItem();
        ArrayList<PackageDTO> levelIDS=packageBO.getAllPackage();
        for (PackageDTO packageDTO:levelIDS){
            if (packageDTO.getPackageName().equalsIgnoreCase(packageName)){
                txtTourBeginDate.setText(packageDTO.getTourBeginDate());
                txtTourEndDate.setText(packageDTO.getTourEndDate());
                txtTourDuration.setText(Integer.toString(packageDTO.getTourDuration()));
                txtPackagePrice.setText(Double.toString(packageDTO.getPackagePrice()));
            }
        }
    }

    @FXML
    void onBooking(ActionEvent event) throws Exception {
        if (ValidationBooking()) {
            double price = Double.parseDouble(txtPayment.getText());
            String date = datePickBookingDate.getText();
            String packageName = comboSelectPackage.getSelectionModel().getSelectedItem();
            int count = Integer.parseInt(txtBookingPersonsCount.getText());
            PaymentDTO paymentDTO = new PaymentDTO(txtPaymentID.getText(), price, date);
            BookingDTO bookingDTO = new BookingDTO(txtBookingID.getText(), txtTravelID.getText(),
                    txtPaymentID.getText(), packageName, date, count);
            CommonBookingDTO dto = new CommonBookingDTO(paymentDTO, bookingDTO);
            if (!(comboSelectPackage.getSelectionModel().getSelectedItem()==null)) {
                boolean result = bookingBO.addBooking(dto);
                if (result) {
                    NotificationController.createSuccesful("Added Successfully", " Booking Added Successfully");
                    clearAll();
                    callAll();
                } else {
                    NotificationController.createError("Added not Successfully", " Booking Added not Successfully");
                }
            }
        }
    }

    private void clearAll() {


        txtPaymentID.clear();
        txtBookingID.clear();
        txtBookingPersonsCount.clear();
        txtPayment.clear();
        txtTourDuration.clear();
        txtTourEndDate.clear();
        txtTourBeginDate.clear();
        txtPackagePrice.clear();

        txtTravelID.clear();
        txtTravelName.clear();
        txtCoutry.clear();
        txtTraavelerMobi.clear();
        travelerEmail.clear();

    }


//    @FXML
//    void toSearch(ActionEvent event) throws Exception {
//        TravelerDTO travelerDTO=travelerBO.searchTraveler(txtSearchTravelerName.getText());
//        if (travelerDTO != null){
//            txtSearcgTravelerID.setText(travelerDTO.getTravelID());
//        }
//    }

    @FXML
    void loadToField(MouseEvent event) throws Exception {
        BookingDTO bookingDTO=tblBooking.getSelectionModel().getSelectedItem();
        txtBookingID.setText(bookingDTO.getBookingID());
        txtPaymentID.setText(bookingDTO.getPaymentid());
        comboSelectPackage.getSelectionModel().select(bookingDTO.getPackageName());
        ArrayList<PackageDTO> allPackage = packageBO.getAllPackage();
        for(PackageDTO packageDTO:allPackage) {
            if (bookingDTO.getPackageName().equals(packageDTO.getPackageName())) {
                txtTourDuration.setText(packageDTO.getTourDuration()+"");
                txtTourEndDate.setText(packageDTO.getTourEndDate());
                txtTourBeginDate.setText(packageDTO.getTourBeginDate());
                txtPackagePrice.setText(packageDTO.getPackagePrice()+"");
            }
        }

        ArrayList<TravelerDTO> allTravelers = travelerBO.getAllTravelers();
        for (TravelerDTO travelerDTO:allTravelers){
            if (bookingDTO.getTravelID().equals(travelerDTO.getTravelID())){
                txtTravelID.setText(travelerDTO.getTravelID());
                txtTravelName.setText(travelerDTO.getTravelerFullName());
                txtCoutry.setText(travelerDTO.getTravelerCountry());
                txtTraavelerMobi.setText(travelerDTO.getTravelerMobileNumber());
                travelerEmail.setText(travelerDTO.getTravelerEmail());
            }
        }


//        ArrayList<PaymentDTO> allPayment = paymentBO.getAllPayment();
//        for (PaymentDTO paymentDTO:allPayment){
//            if (paymentDTO.getPaymentID().equals(bookingDTO.getPaymentid())){
//                txtPaymentID.setText();
//            }
//
//        }


    }
    @FXML
    void onDeleteBooking(ActionEvent event) throws Exception {
        if (ValidationBooking() ) {
            boolean result = bookingBO.deleteBooking(txtBookingID.getText());
            if (result){
                NotificationController.createSuccesful("Deleted Change","Deleted Sucess");
                clearAll();
                callAll();
            }else{
                NotificationController.createError("Deleted Change","Deleted UnSucess");
            }
        }
    }

    @FXML
    void onUpdateBooking(ActionEvent event) throws Exception {
        if (ValidationBooking()) {


            boolean result = bookingBO.updateBooking(new BookingDTO(
                    txtBookingID.getText(),
                    txtTravelID.getText(),
                    txtPaymentID.getText(),
                    comboSelectPackage.getSelectionModel().getSelectedItem(),
                    datePickBookingDate.getText(),
                    Integer.parseInt(txtBookingPersonsCount.getText())
            ));
            if (result){
                clearAll();
                callAll();
                NotificationController.createSuccesful("Update Change","Updated Sucess");
            }else {
                NotificationController.createError("Update Change","Updated UnSucess");
            }
        }
    }

    private void timeAndDate() {
        Timeline time = new Timeline(new KeyFrame(Duration.seconds(0), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                datePickBookingDate.setText(new SimpleDateFormat("YYYY-MM-dd").format(new Date()));
            }
        }), new KeyFrame(Duration.seconds(1)));
        time.setCycleCount(Animation.INDEFINITE);
        time.play();
    }

    private void setNewPID() throws Exception {
        String chid;
        chid= IDGenarator.getNewID("Payment","paymentID","P");
        txtPaymentID.setText(chid);
    }

    @FXML
    void getPaymentText(ActionEvent event) {


        if (new Validation().onlyNumbers(txtBookingPersonsCount.getText())) {
            double price = Double.parseDouble(txtPackagePrice.getText());
            int a = Integer.parseInt(txtBookingPersonsCount.getText());
            double pay = price * a;
            txtPayment.setText(Double.toString(pay));
        } else {
            NotificationController.createError("Write Count of People","Check and Re Write");

        }
    }
    private boolean ValidationTraveler(){
        boolean rest=true;
        String travelerName = txtTravelName.getText();
        boolean result = new Validation().validateName(travelerName);
        if (result){
            txtTravelName.setStyle("-fx-background-color: white");
            txtCoutry.requestFocus();
        }else {
            rest=false;
            txtTravelName.setStyle("-fx-background-color: red");
            txtTravelName.requestFocus();
        }
        String coutryText = txtCoutry.getText();
        if (new Validation().validateName(coutryText)){

            txtCoutry.setStyle("-fx-background-color: white");
            txtTraavelerMobi.requestFocus();
        }else{
            rest=false;
            txtCoutry.setStyle("-fx-background-color: red");
            txtCoutry.requestFocus();
        }

        if (new Validation().validateContactNumber(txtTraavelerMobi.getText())){

            txtTraavelerMobi.setStyle("-fx-background-color: white");
            travelerEmail.requestFocus();
        }else{
            rest=false;
            txtTraavelerMobi.setStyle("-fx-background-color: red");
            txtTraavelerMobi.requestFocus();
        }
        if (new Validation().email(travelerEmail.getText())){

            travelerEmail.setStyle("-fx-background-color: white");
            btnAdd.requestFocus();
        }else{
            rest=false;
            travelerEmail.setStyle("-fx-background-color: red");
            travelerEmail.requestFocus();
            txtTravelName.requestFocus();
        }

        return rest;
    }

    private boolean ValidationBooking(){
        boolean result=true;
        if (new Validation().date(txtTourBeginDate.getText())){
            txtTourEndDate.setStyle("-fx-background-color: white");
            txtPackagePrice.setStyle("-fx-background-color: white");
            txtTourDuration.setStyle("-fx-background-color: white");
            txtPayment.setStyle("-fx-background-color: white");
            txtTourBeginDate.setStyle("-fx-background-color: white");
            txtBookingPersonsCount.setStyle("-fx-background-color:#8B8E8F ");
        }else{
            result=false;
            txtTourBeginDate.setStyle("-fx-background-color: red");
            txtPackagePrice.setStyle("-fx-background-color: red");
            txtTourDuration.setStyle("-fx-background-color: red");
            txtPayment.setStyle("-fx-background-color: red");
            txtTourEndDate.setStyle("-fx-background-color: red");
            txtBookingPersonsCount.setStyle("-fx-background-color: red");
            NotificationController.createError("Selection Error","Select Package First!");
        }


      return result;
    }

    public void onActionbtntrvelerUpdate(ActionEvent actionEvent) throws Exception {
        if (ValidationTraveler()){
            TravelerDTO travelerDTO=new TravelerDTO(
                    txtTravelID.getText(),
                    txtTravelName.getText(),
                    txtCoutry.getText(),
                    txtTraavelerMobi.getText(),
                    travelerEmail.getText()
            );
            boolean result = travelerBO.updateTraveler(travelerDTO);
            if (result){
                clearAll();
                callAll();
                NotificationController.createSuccesful("Update Successfully", " Traveler Update Successfully");
            }else{
                NotificationController.createError("Update not Successfully", " Updated not Successfully");
            }
        }
    }

    public void onActionBtnTravelerDel(ActionEvent actionEvent) throws Exception {
        if (ValidationTraveler()){
            boolean result = travelerBO.deleteTraveler(txtTravelID.getText());
            if (result){
                clearAll();
                callAll();
                NotificationController.createSuccesful("Deleted Successfully", " Traveler Deleted Successfully");
            }else{
                NotificationController.createError("Deleted not Successfully", " Deleted not Successfully");
            }
        }
    }

    private boolean searchName(String name) throws Exception {
        boolean result = true;
        ArrayList<TravelerDTO> allTravelers = travelerBO.getAllTravelers();
        for (TravelerDTO travelerDTO: allTravelers) {
            if (name.equalsIgnoreCase(travelerDTO.getTravelerFullName())){
                txtTravelID.setText(travelerDTO.getTravelID());
                txtTraavelerMobi.setText(travelerDTO.getTravelerMobileNumber());
                txtCoutry.setText(travelerDTO.getTravelerCountry());
                travelerEmail.setText(travelerDTO.getTravelerEmail());
                result=false;
            }
        }
        return result;
    }

    public void onActionTravelerName(ActionEvent actionEvent) throws Exception {
        String travelerName = txtTravelName.getText();
        boolean result = new Validation().validateName(travelerName);
        if (result){
            boolean a = searchName(travelerName);
            if (a) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "No Like This", ButtonType.OK);
                alert.show();
                txtCoutry.requestFocus();
            }
            txtTravelName.setStyle("-fx-background-color: white");
            txtCoutry.requestFocus();
        }else {
            txtTravelName.setStyle("-fx-background-color: red");
            txtTravelName.requestFocus();
        }


    }

    @FXML
    void onActionCountry(ActionEvent event) {
        String coutryText = txtCoutry.getText();
        boolean result = new Validation().validateName(coutryText);
        if (result){
            txtCoutry.setStyle("-fx-background-color: white");
            txtTraavelerMobi.requestFocus();
        }else{
            txtCoutry.setStyle("-fx-background-color: red");
            txtCoutry.requestFocus();
        }
    }

    @FXML
    void onActionEmail(ActionEvent event) {
        boolean result = new Validation().email(travelerEmail.getText());
        if (result){
            travelerEmail.setStyle("-fx-background-color: white");
            btnAdd.requestFocus();
        }else{
            travelerEmail.setStyle("-fx-background-color: red");
            travelerEmail.requestFocus();
        }
    }

    @FXML
    void onActionMobileNumber(ActionEvent event) {
        boolean result = new Validation().validateContactNumber(txtTraavelerMobi.getText());
        if (result){
            txtTraavelerMobi.setStyle("-fx-background-color: white");
            travelerEmail.requestFocus();
        }else{
            txtTraavelerMobi.setStyle("-fx-background-color: red");
            txtTraavelerMobi.requestFocus();
        }
    }
}
