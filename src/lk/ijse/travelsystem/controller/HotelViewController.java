package lk.ijse.travelsystem.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import lk.ijse.travelsystem.bo.custom.HotelBO;
import lk.ijse.travelsystem.bo.custom.MainLocationBO;
import lk.ijse.travelsystem.bo.custom.impl.HotelBOImpl;
import lk.ijse.travelsystem.bo.custom.impl.MainLocationBOImpl;
import lk.ijse.travelsystem.common.NotificationController;
import lk.ijse.travelsystem.common.Validation;
import lk.ijse.travelsystem.model.HotelDTO;
import lk.ijse.travelsystem.model.MainLocationDTO;


import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class HotelViewController implements Initializable {

    @FXML
    private JFXTextField txtHotelName;

    @FXML
    private JFXComboBox<String> comboMainLocation;

    @FXML
    private JFXTextField txtHotelAddress;

    @FXML
    private JFXTextField txtHotelLocation;

    @FXML
    private JFXTextField txtHotelTele;

    @FXML
    private JFXComboBox<String> comboSelectLocaton;

    @FXML
    private TableView<HotelDTO> tblHotels;

    @FXML
    private JFXButton btnUpdateHotel;

    @FXML
    private JFXButton btnDelete;

    @FXML
    private JFXButton btnadd;

    private static HotelBO hotelBO=new HotelBOImpl();
    private static MainLocationBO locationBO=new MainLocationBOImpl();

    @FXML
    void onAddHotel(ActionEvent event) throws Exception {
        if (Validation()){
            String mainLocation=comboMainLocation.getSelectionModel().getSelectedItem();
            HotelDTO hotelDTO=new HotelDTO(
                    1,
                    mainLocation,
                    txtHotelName.getText(),
                    txtHotelAddress.getText(),
                    txtHotelLocation.getText(),
                    txtHotelTele.getText()
            );
            boolean result = hotelBO.addHotel(hotelDTO);
            if (result){
                NotificationController.createSuccesful("Added Successfully", " Hotel Added Successfully");
                loadMainLocations();
                txtHotelName.clear();
                txtHotelAddress.clear();
                txtHotelLocation.clear();
                txtHotelTele.clear();
            }else{
                NotificationController.createError("Added not Successfully", " Added not Successfully");
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            loadMainLocations();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void onSelectLocation(ActionEvent event) throws Exception {
        loadDetailsHotel();
    }

    @FXML
    void selectHotel(MouseEvent event) {
        HotelDTO hotelDTO=tblHotels.getSelectionModel().getSelectedItem();
        txtHotelName.setText(hotelDTO.getHotelName());
        txtHotelAddress.setText(hotelDTO.getHotelAddress());
        txtHotelLocation.setText(hotelDTO.getHotelLocation());
        txtHotelTele.setText(hotelDTO.getHotelTel());
        comboMainLocation.setValue(hotelDTO.getMainLocationName());
    }

    @FXML
    void onDelete(ActionEvent event) throws Exception {
        boolean result = hotelBO.deleteHotel(txtHotelName.getText());
        if (result){
            NotificationController.createSuccesful("Deleted Successfully", " Hotel Deleted Successfully");
        }else{
            NotificationController.createError("Deleted not Successfully", " Update Deleted Successfully");
        }
    }

    @FXML
    void onUpdate(ActionEvent event) throws Exception {
        String mainLocation=comboMainLocation.getSelectionModel().getSelectedItem();
        HotelDTO hotelDTO=new HotelDTO(
                0,
                mainLocation,
                txtHotelName.getText(),
                txtHotelAddress.getText(),
                txtHotelLocation.getText(),
                txtHotelTele.getText()
        );
        boolean result = hotelBO.updateHotel(hotelDTO);
        if (result){
            NotificationController.createSuccesful("Updated Successfully", " Hotel Updated Successfully");
        }else{
            NotificationController.createError("Updated not Successfully", " Update not Successfully");
        }
    }

    private void loadMainLocations() throws Exception {
        ArrayList<MainLocationDTO> levelIDS=locationBO.getAllMainLocation();
        ArrayList<String>ids=new ArrayList<>();
        for (MainLocationDTO dto:levelIDS) {
            ids.add(dto.getMainLocationName());
        }
        comboMainLocation.getItems().addAll(ids);
        comboSelectLocaton.getItems().addAll(ids);
    }

    private void loadDetailsHotel() throws Exception {
        String mainLocation=comboSelectLocaton.getSelectionModel().getSelectedItem();
        tblHotels.setItems(FXCollections.observableArrayList(hotelBO.getAllHotelsForLocation(mainLocation)));

        tblHotels.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("hotelID"));
        tblHotels.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("mainLocationName"));
        tblHotels.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("hotelName"));
        tblHotels.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("hotelAddress"));
        tblHotels.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("hotelLocation"));
        tblHotels.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("hotelTel"));

    }

    private boolean Validation(){
        boolean result =true;

        if (new Validation().validateName(txtHotelAddress.getText())){
            txtHotelAddress.setStyle("-fx-background-color: white");
            txtHotelTele.requestFocus();
        }else {
            result=false;
            txtHotelAddress.setStyle("-fx-background-color: red");
            txtHotelAddress.requestFocus();
        }


        if (new Validation().validateName(txtHotelName.getText())){
            txtHotelName.setStyle("-fx-background-color: white");
            txtHotelAddress.requestFocus();
        }else{
            result=false;
            txtHotelName.setStyle("-fx-background-color: red");
            txtHotelName.requestFocus();
        }


        if (new Validation().validateContactNumber(txtHotelTele.getText())){
            txtHotelTele.setStyle("-fx-background-color: white");
            btnadd.requestFocus();
        }else{
            result=false;
            txtHotelTele.setStyle("-fx-background-color: red");
            txtHotelTele.requestFocus();
        }

        if (new Validation().validateName(txtHotelLocation.getText())){
            txtHotelLocation.setStyle("-fx-background-color: white");
        }else{
            result=false;
            NotificationController.createError("Select Package","Pacakge Selection Failed !");
        }

        return result;
    }

    @FXML
    void onactioncomboMainLocation(ActionEvent event) {
        String selectedItem =comboMainLocation.getSelectionModel().getSelectedItem();
        txtHotelLocation.setText(selectedItem);
    }

    @FXML
    void onActionHotelAddress(ActionEvent event) {
        if (new Validation().validateName(txtHotelAddress.getText())){
            txtHotelAddress.setStyle("-fx-background-color: white");
            txtHotelTele.requestFocus();
        }else {
            txtHotelAddress.setStyle("-fx-background-color: red");
            txtHotelAddress.requestFocus();
        }
    }

    @FXML
    void onActionHotelName(ActionEvent event) {
        if (new Validation().validateName(txtHotelName.getText())){
            txtHotelName.setStyle("-fx-background-color: white");
            txtHotelAddress.requestFocus();
        }else{
            txtHotelName.setStyle("-fx-background-color: red");
            txtHotelName.requestFocus();
        }
    }

    @FXML
    void onActionTephonenumber(ActionEvent event) {
        if (new Validation().validateContactNumber(txtHotelTele.getText())){
            txtHotelTele.setStyle("-fx-background-color: white");
            btnadd.requestFocus();
        }else{
            txtHotelTele.setStyle("-fx-background-color: red");
            txtHotelTele.requestFocus();
        }
    }
}
