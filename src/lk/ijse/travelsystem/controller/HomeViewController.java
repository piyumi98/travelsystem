package lk.ijse.travelsystem.controller;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;

public class HomeViewController implements Initializable {

    @FXML
    private AnchorPane loadPane;

    @FXML
    private Label lblDate;

    @FXML
    private Label lblTime;

    @FXML
    private Label lblDashboard;

    @FXML
    private Label lblTraveler;

    @FXML
    private Label lblPackage;

    @FXML
    private Label lblHotel;

    @FXML
    private Label lblLocation;

    @FXML
    private Label lblLogOut;


    @FXML
    private Label lblLoadTraveler;

    @FXML
    private Label lblLoadPackage;

    @FXML
    private Label lblLoadHotel;

    @FXML
    private Label lblLoadLocation;

    @FXML
    private Label lblLoadDashboard;

    @FXML
    private Label lblLoadLogout;

    @FXML
    private AnchorPane root;


    private static final String IDLE_BUTTON_STYLE = "-fx-background-color: red;-fx-text-fill: white;-fx-alignment: center;";
    private static final String HOVERED_BUTTON_STYLE = "-fx-background-color: white; -fx-text-fill: black; -fx-alignment: center;";

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        timeAndDate();

        try {
            AnchorPane parenRoot=parenRoot = FXMLLoader.load(this.getClass().getResource("/lk/ijse/travelsystem/view/dashboardView.fxml"));
            Scene scene=new Scene(parenRoot);
            loadPane.getChildren().setAll(parenRoot);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void timeAndDate() {
        Timeline time = new Timeline(new KeyFrame(Duration.seconds(0), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lblTime.setText(new SimpleDateFormat("hh:mm:ss a").format(new Date()));
                lblDate.setText(new SimpleDateFormat("YYYY-MM-dd").format(new Date()));
            }
        }), new KeyFrame(Duration.seconds(1)));
        time.setCycleCount(Animation.INDEFINITE);
        time.play();
    }

    @FXML
    void DashboardEnterd(MouseEvent event) {
        lblLoadDashboard.setStyle(IDLE_BUTTON_STYLE);
    }

    @FXML
    void DashboardExited(MouseEvent event) {
        lblLoadDashboard.setStyle(HOVERED_BUTTON_STYLE);
    }

    @FXML
    void TravelerEnterd(MouseEvent event) {
        lblLoadTraveler.setStyle(IDLE_BUTTON_STYLE);
    }

    @FXML
    void TravelerExited(MouseEvent event) {
        lblLoadTraveler.setStyle(HOVERED_BUTTON_STYLE);
    }

    @FXML
    void hotelEntered(MouseEvent event) {
        lblLoadHotel.setStyle(IDLE_BUTTON_STYLE);
    }

    @FXML
    void hotelExited(MouseEvent event) {
        lblLoadHotel.setStyle(HOVERED_BUTTON_STYLE);
    }

    @FXML
    void locationEntered(MouseEvent event) {
        lblLoadLocation.setStyle(IDLE_BUTTON_STYLE);
    }

    @FXML
    void locationExited(MouseEvent event) {
        lblLoadLocation.setStyle(HOVERED_BUTTON_STYLE);
    }

    @FXML
    void logoutEntered(MouseEvent event) {
        lblLoadLogout.setStyle(IDLE_BUTTON_STYLE);
    }

    @FXML
    void logoutExited(MouseEvent event) {
        lblLoadLogout.setStyle(HOVERED_BUTTON_STYLE);
    }

    @FXML
    void packageEntered(MouseEvent event) {
        lblLoadPackage.setStyle(IDLE_BUTTON_STYLE);
    }

    @FXML
    void packageExited(MouseEvent event) {
        lblLoadPackage.setStyle(HOVERED_BUTTON_STYLE);
    }

    @FXML
    void onCLickTraveler(MouseEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/lk/ijse/travelsystem/view/travelerView.fxml"));
        loadPane.getChildren().setAll(pane);
    }

    @FXML
    void onclickMaximize(MouseEvent event) {

    }

    @FXML
    void onClickClose(MouseEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Look, a Confirmation Dialog");
        alert.setContentText("Are you sure?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            System.exit(0);
        } else {
        }
    }

    @FXML
    void onCLickMinize(MouseEvent event) {
        Stage stage = (Stage) ((ImageView) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    void onClickDashboard(MouseEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/lk/ijse/travelsystem/view/dashboardView.fxml"));
        loadPane.getChildren().setAll(pane);
    }

    @FXML
    void onClickHOtel(MouseEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/lk/ijse/travelsystem/view/hotelView.fxml"));
        loadPane.getChildren().setAll(pane);
    }

    @FXML
    void onClickLocation(MouseEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/lk/ijse/travelsystem/view/Locatins.fxml"));
        loadPane.getChildren().setAll(pane);
    }

    @FXML
    void onClickLogout(MouseEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Look, Login out From This!");
        alert.setContentText("Are you sure?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Parent parentRoot = FXMLLoader.load(this.getClass().getResource("/lk/ijse/travelsystem/view/loginView.fxml"));
            Scene scene = new Scene(parentRoot);

            Stage window = (Stage) this.root.getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();

            TranslateTransition tran = new TranslateTransition(Duration.millis(1000), scene.getRoot());
            tran.setToX(0);
            tran.setFromX(-scene.getWidth());
            tran.play();
        } else {
        }

    }

    @FXML
    void onClickPackage(MouseEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/lk/ijse/travelsystem/view/pakageDetailView.fxml"));
        loadPane.getChildren().setAll(pane);
    }
}
