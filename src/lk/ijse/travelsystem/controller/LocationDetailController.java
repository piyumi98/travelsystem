package lk.ijse.travelsystem.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import lk.ijse.travelsystem.bo.custom.LocationDetailBO;
import lk.ijse.travelsystem.bo.custom.MainLocationBO;
import lk.ijse.travelsystem.bo.custom.SubLocationBO;
import lk.ijse.travelsystem.bo.custom.impl.LocationDetailBOImpl;
import lk.ijse.travelsystem.bo.custom.impl.MainLocationBOImpl;
import lk.ijse.travelsystem.bo.custom.impl.SubLocationBOImpl;
import lk.ijse.travelsystem.common.NotificationController;
import lk.ijse.travelsystem.common.Validation;
import lk.ijse.travelsystem.model.LocationDetailDTO;
import lk.ijse.travelsystem.model.MainLocationDTO;
import lk.ijse.travelsystem.model.SubLocationDTO;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class LocationDetailController implements Initializable {

    @FXML
    private JFXTextField txtMainLocationName;

    @FXML
    private JFXTextArea txtAreaLocationDescription;

    @FXML
    private JFXTextField txtLocationDistance;

    @FXML
    private JFXTextField txtAverageTime;

    @FXML
    private JFXComboBox<String> comboMainLocation;

    @FXML
    private JFXComboBox<String> comboSubLocation;

    @FXML
    private JFXTextField txtSubLocationDistance;

    @FXML
    private TableView<LocationDetailDTO> tblDetails;

    @FXML
    private JFXTextField txtSubLocationName;

    @FXML
    private JFXTextField txtSubDistance;


    @FXML
    private JFXButton btnAdd;

    @FXML
    private JFXTextField txtViewDistance;

    @FXML
    private JFXTextField txtViewAverageTime;

    @FXML
    private JFXTextArea txtViewDescription;

    private static MainLocationBO locationBO=new MainLocationBOImpl();
    private static SubLocationBO bo=new SubLocationBOImpl();
    private static LocationDetailBO detailBO=new LocationDetailBOImpl();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            loadMainLocations();
            loadSubLocations();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void toAddLocation(ActionEvent event) throws Exception {
        if (ValidationMainLocation()){
            MainLocationDTO mainLocationDTO=new MainLocationDTO(
                    txtMainLocationName.getText(),
                    txtAreaLocationDescription.getText(),
                    Double.parseDouble(txtLocationDistance.getText()),
                    Integer.parseInt(txtAverageTime.getText())
            );
            boolean result = locationBO.addMainLocation(mainLocationDTO);
            if (result){
                loadAllMainLocations();
                NotificationController.createSuccesful("Added Successfully", " Main Location Added Successfully");
                txtMainLocationName.clear();
                txtAreaLocationDescription.clear();
                txtLocationDistance.clear();
                txtAverageTime.clear();
            }else{
                NotificationController.createError("Added not Successfully", " Added not Successfully");
            }
        }
    }

    private void loadMainLocations() throws Exception {
        ArrayList<MainLocationDTO> levelIDS=locationBO.getAllMainLocation();
        ArrayList<String>ids=new ArrayList<>();
        for (MainLocationDTO dto:levelIDS) {
            ids.add(dto.getMainLocationName());
        }
        comboMainLocation.getItems().addAll(ids);
    }

    private void loadAllMainLocations() throws Exception {
        ArrayList<MainLocationDTO> levelIDS=locationBO.getAllMainLocation();
        ArrayList<String>ids=new ArrayList<>();
        for (MainLocationDTO dto:levelIDS) {
            ids.add(dto.getMainLocationName());
        }
        comboMainLocation.getItems().addAll(ids);
    }

    private void loadSubLocations() throws Exception {
        ArrayList<SubLocationDTO> levelIDS=bo.getAllMainLocation();
        ArrayList<String>ids=new ArrayList<>();
        for (SubLocationDTO dto:levelIDS) {
            ids.add(dto.getSublocationName());
        }
        comboSubLocation.getItems().addAll(ids);
    }

    private void loadAllSubLocations() throws Exception {
        ArrayList<SubLocationDTO> levelIDS=bo.getAllMainLocation();
        ArrayList<String>ids=new ArrayList<>();
        for (SubLocationDTO dto:levelIDS) {
            ids.add(dto.getSublocationName());
        }
        comboSubLocation.getItems().addAll(ids);
    }

    @FXML
    void loadDetails(ActionEvent event)  throws Exception {
        ArrayList<MainLocationDTO> levelIDS=locationBO.getAllMainLocation();
        String mainLocation=comboMainLocation.getSelectionModel().getSelectedItem();
        for (MainLocationDTO dto:levelIDS) {
            if (dto.getMainLocationName().equalsIgnoreCase( mainLocation)){
                txtViewDescription.setText(dto.getLocationDescption());
                txtViewDistance.setText(Double.toString(dto.getLocationDIstance()));
                txtViewAverageTime.setText(Integer.toString(dto.getAvgTimeTOGOhours()));
            }
        }
    }

    @FXML
    void btnAddTable(ActionEvent event) {

        String mainLocation=comboMainLocation.getSelectionModel().getSelectedItem();
        String subLocation=comboSubLocation.getSelectionModel().getSelectedItem();

        LocationDetailDTO dto=new LocationDetailDTO(mainLocation,subLocation);
        tblDetails.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("mainLocationName"));
        tblDetails.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("sublocationName"));

        tblDetails.getItems().add(dto);

    }

    @FXML
    void onAddSubLocation(ActionEvent event) throws Exception {

        boolean result=false;
        for (int i=0;i<tblDetails.getItems().size();i++){
            String mainLocation= (String) tblDetails.getColumns().get(0).getCellObservableValue(i).getValue();
            String subLocation=(String) tblDetails.getColumns().get(1).getCellObservableValue(i).getValue();
            LocationDetailDTO locationDTOS=new LocationDetailDTO(mainLocation,subLocation);
            result=detailBO.addLocationDetail(locationDTOS);
        }
        if (result){
            NotificationController.createSuccesful("Added Successfully", " Added Successfully");
        }else{
            NotificationController.createError("Added not Successfully", " Added not Successfully");
        }
    }

    @FXML
    void addToSubLocation(ActionEvent event) throws Exception {
        if (validationSubLocation()){
            SubLocationDTO subLocationDTO=new SubLocationDTO(txtSubLocationName.getText(),Double.parseDouble(txtSubDistance.getText()));
            boolean isAdd=bo.addSubLocation(subLocationDTO);
            if (isAdd){
                loadAllSubLocations();
                NotificationController.createSuccesful("Added Successfully", " Sub Location Added Successfully");
                txtSubLocationName.clear();
                txtSubLocationDistance.clear();
            }else{
                NotificationController.createError("Added not Successfully", " Added not Successfully");
            }
        }
    }

    @FXML
    void toLoadSubDetails(ActionEvent event) throws Exception {
        ArrayList<SubLocationDTO> levelIDS=bo.getAllMainLocation();
        String subLocation=comboSubLocation.getSelectionModel().getSelectedItem();
        for (SubLocationDTO dto:levelIDS) {
            if (dto.getSublocationName().equalsIgnoreCase( subLocation)){
                txtSubLocationDistance.setText(Double.toString(dto.getLocationDIstance()));
            }
        }
    }

    private boolean ValidationMainLocation(){
//        if(txtMainLocationName.getText().trim().isEmpty()){
//            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Main Location Name..", ButtonType.OK);
//            a.show();
//            txtMainLocationName.requestFocus();
//            return false;
//        }else if(txtAreaLocationDescription.getText().trim().isEmpty()){
//            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Location Description..",ButtonType.OK);
//            a.show();
//            txtAreaLocationDescription.requestFocus();
//            return false;
//        }else if(txtLocationDistance.getText().trim().isEmpty()){
//            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Location Distance..",ButtonType.OK);
//            a.show();
//            txtLocationDistance.requestFocus();
//            return false;
//        }else if(txtAverageTime.getText().trim().isEmpty()){
//            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Average Time..",ButtonType.OK);
//            a.show();
//            txtAverageTime.requestFocus();
//            return false;
//        }
//        return true;
        boolean result = true;
        if (new Validation().onlyNumbers(txtAverageTime.getText())){
            txtAverageTime.setStyle("-fx-background-color: white");
            txtAreaLocationDescription.requestFocus();
        }else{
            result=false;
            txtAverageTime.setStyle("-fx-background-color: red");
            txtAverageTime.requestFocus();
        }


        if (new Validation().onlyNumbers(txtLocationDistance.getText())){
            txtLocationDistance.setStyle("-fx-background-color: white");
            txtAverageTime.requestFocus();
        }else{
            result=false;
            txtLocationDistance.setStyle("-fx-background-color: red");
            txtLocationDistance.requestFocus();
        }


        if (new Validation().validateName(txtMainLocationName.getText())){
            txtMainLocationName.setStyle("-fx-background-color: white");
            txtLocationDistance.requestFocus();
        }else{
            result=false;
            txtMainLocationName.setStyle("-fx-background-color: red");
            txtMainLocationName.requestFocus();
        }


        return result;
    }

    private boolean validationSubLocation(){
        if(txtSubLocationName.getText().trim().isEmpty()){
            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Sub Location Name..", ButtonType.OK);
            a.show();
            txtSubLocationName.requestFocus();
            return false;
        }else if(txtSubDistance.getText().trim().isEmpty()){
            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Sub Location Distance..",ButtonType.OK);
            a.show();
            txtSubDistance.requestFocus();
            return false;
        }
        return true;
    }


    @FXML
    void onActionAvgtoGo(ActionEvent event) {
        if (new Validation().onlyNumbers(txtAverageTime.getText())){
            txtAverageTime.setStyle("-fx-background-color: white");
            txtAreaLocationDescription.requestFocus();
        }else{
            txtAverageTime.setStyle("-fx-background-color: red");
            txtAverageTime.requestFocus();
        }
    }

    @FXML
    void onActionDistancnce(ActionEvent event) {
        if (new Validation().onlyNumbers(txtLocationDistance.getText())){
            txtLocationDistance.setStyle("-fx-background-color: white");
            txtAverageTime.requestFocus();
        }else{
            txtLocationDistance.setStyle("-fx-background-color: red");
            txtLocationDistance.requestFocus();
        }
    }

    @FXML
    void onActionMainLocationName(ActionEvent event) {
        if (new Validation().validateName(txtMainLocationName.getText())){
            txtMainLocationName.setStyle("-fx-background-color: white");
            txtLocationDistance.requestFocus();
        }else{
            txtMainLocationName.setStyle("-fx-background-color: red");
            txtMainLocationName.requestFocus();
        }
    }

}
