package lk.ijse.travelsystem.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lk.ijse.travelsystem.bo.custom.*;
import lk.ijse.travelsystem.bo.custom.impl.*;
import lk.ijse.travelsystem.common.NotificationController;
import lk.ijse.travelsystem.common.Validation;
import lk.ijse.travelsystem.model.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class PakageDetailViewController implements Initializable {

    @FXML
    private JFXTextField txtGuideName;

    @FXML
    private JFXTextField txtGuideAddress;

    @FXML
    private JFXTextField txtGuideTele;

    @FXML
    private JFXButton btnAdd;

    @FXML
    private JFXButton btnUpdate;

    @FXML
    private JFXComboBox<GuideDTO> comboGuideName;

    @FXML
    private JFXComboBox<String> comboLangugae;

    @FXML
    private TableView<GuideDetailsDTO> tblGuideDetails;

    @FXML
    private JFXButton btnAddToChart;

    @FXML
    private JFXButton btnAddDetails;

    @FXML
    private JFXButton btnSearch;

    @FXML
    private JFXTextField txtPackageName;

    @FXML
    private JFXComboBox<GuideDTO> comboSelectGuideName;

    @FXML
    private DatePicker dateTourStart;

    @FXML
    private DatePicker dateTourEnd;

    @FXML
    private JFXTextField txtDuration;

    @FXML
    private JFXTextField txtPackagePrice;

    @FXML
    private JFXButton btnCreatePackage;

    @FXML
    private JFXComboBox<HotelDTO> comboSelectHotelName;

    @FXML
    private TableView<PackageDTO> tblPackage;

    private static GuideBO guideBO=new GuideBOImpl();
    private static LanguagesBO languagesBO=new LanguagesBOImpl();
    private static GuideDetailBO detailBO=new GuideDetailBOImpl();
    private static HotelBO hotelBO=new HotelBOImpl();
    private static PackageBO  packageBO=new PackageBOImpl();

    @FXML
    void onAddDetails(ActionEvent event) throws Exception {
        boolean result=false;
        for (int i=0;i<tblGuideDetails.getItems().size();i++){
            int guideID= Integer.parseInt(String.valueOf(tblGuideDetails.getColumns().get(0).getCellObservableValue(i).getValue()));
            String lang=(String) tblGuideDetails.getColumns().get(1).getCellObservableValue(i).getValue();

            GuideDetailsDTO detailsDTO=new GuideDetailsDTO(guideID,lang);
            result=detailBO.addGuideDetails(detailsDTO);
        }
        if (result){
            NotificationController.createSuccesful("Added Successfully", " Added Successfully");
            loadHotels();
        }else{
            NotificationController.createError("Added not Successfully", " Added not Successfully");
        }
    }

    @FXML
    void onAddGuide(ActionEvent event) throws Exception {
        if (ValidationGuides()){
            GuideDTO guideDTO=new GuideDTO(1,txtGuideName.getText(),txtGuideAddress.getText(),txtGuideTele.getText());
            boolean isAdd=guideBO.addGuide(guideDTO);
            if (isAdd){
                NotificationController.createSuccesful("Added Successfully", " Guide Added Successfully");
                loadGuideID();
                txtGuideName.clear();
                txtGuideAddress.clear();
                txtGuideTele.clear();
            }else{
                NotificationController.createError("Added not Successfully", " Added not Successfully");
            }
        }
    }

    @FXML
    void onCreatePackage(ActionEvent event) throws Exception {
        if (ValidationPackage()){
            int id=comboSelectGuideName.getSelectionModel().getSelectedItem().getGuideID();
            int hotelID=comboSelectHotelName.getSelectionModel().getSelectedItem().getHotelID();
            String startDate= String.valueOf(dateTourStart.getValue());
            String endDate= String.valueOf(dateTourEnd.getValue());
            int duration=Integer.parseInt(txtDuration.getText());
            double price=Double.parseDouble(txtPackagePrice.getText());
            PackageDTO aPackage=new PackageDTO(txtPackageName.getText(),id,startDate,endDate,duration,price);
            HotelDetailDTO detailDTO=new HotelDetailDTO(hotelID,txtPackageName.getText());
            CommonDTO commonDTO=new CommonDTO(aPackage,detailDTO);
            boolean isAdded=packageBO.addPackage(commonDTO);
            if (isAdded){
                loadPackages();
                NotificationController.createSuccesful("Added Successfully", " Added New Package Successfully");
            }else{
                NotificationController.createError("Added not Successfully", " Added not Successfully");
            }
        }
    }

    @FXML
    void onSearch(ActionEvent event) throws Exception {
        GuideDTO guideDTO=guideBO.searchGuide(txtGuideName.getText());
        if (guideDTO != null){
            txtGuideAddress.setText(guideDTO.getGuideAddress());
            txtGuideTele.setText(guideDTO.getGuideTelNo());
        }
    }

    @FXML
    void onUpdateGuide(ActionEvent event) throws Exception {
        GuideDTO guideDTO=new GuideDTO(1,txtGuideName.getText(),txtGuideAddress.getText(),txtGuideTele.getText());
        boolean isAdd=guideBO.updateGuide(guideDTO);
        if (isAdd){
            NotificationController.createSuccesful("Update Successfully", " Update Successfully");
        }else{
            NotificationController.createError("Update not Successfully", " Update not Successfully");
        }
    }

    @FXML
    void toAddTable(ActionEvent event) {
        int guideID=comboGuideName.getSelectionModel().getSelectedItem().getGuideID();
        String langauge=comboLangugae.getSelectionModel().getSelectedItem();

        GuideDetailsDTO dto=new GuideDetailsDTO(guideID,langauge);
        tblGuideDetails.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("guideID"));
        tblGuideDetails.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("langName"));

        tblGuideDetails.getItems().add(dto);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        tblPackage.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("packageName"));
        tblPackage.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("guideID"));
        tblPackage.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("tourBeginDate"));
        tblPackage.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("tourEndDate"));
        tblPackage.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("tourDuration"));
        tblPackage.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("packagePrice"));

        try {
            loadGuideID();
            loadLanguages();
            loadHotels();
            loadPackages();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadPackages() throws Exception {
        tblPackage.setItems(FXCollections.observableArrayList(packageBO.getAllPackage()));
    }

    private void loadHotels() throws Exception {
        ArrayList<HotelDTO> levelIDS=hotelBO.getAllHotels();
        ArrayList<HotelDTO>ids=new ArrayList<>();
        for (HotelDTO dto:levelIDS) {
            ids.add(new HotelDTO(dto.getHotelID(),dto.getMainLocationName(),dto.getHotelName(),dto.getHotelAddress(),
                    dto.getHotelLocation(),dto.getHotelTel()));
        }
        comboSelectHotelName.getItems().addAll(ids);
    }

    private void loadLanguages() throws Exception {
        ArrayList<LangaugesDTO> levelIDS=languagesBO.getAllMainLocation();
        ArrayList<String>ids=new ArrayList<>();
        for (LangaugesDTO dto:levelIDS) {
            ids.add(dto.getLangName());
        }
        comboLangugae.getItems().addAll(ids);
    }

    private void loadGuideID() throws Exception {
        ArrayList<GuideDTO> levelIDS=guideBO.getAllGuides();
        ArrayList<GuideDTO>ids=new ArrayList<>();
        for (GuideDTO dto:levelIDS) {
            ids.add(new GuideDTO(dto.getGuideID(),dto.getGuideName(),dto.getGuideAddress(),dto.getGuideTelNo()));
        }
        comboGuideName.getItems().addAll(ids);
        comboSelectGuideName.getItems().addAll(ids);
    }

    private boolean ValidationGuides(){
        if(txtGuideName.getText().trim().isEmpty()){
            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Guide Name..", ButtonType.OK);
            a.show();
            txtGuideName.requestFocus();
            return false;
        }else if(txtGuideAddress.getText().trim().isEmpty()){
            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Guide Address..",ButtonType.OK);
            a.show();
            txtGuideAddress.requestFocus();
            return false;
        }else if(txtGuideTele.getText().trim().isEmpty()){
            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Guide Telephone..",ButtonType.OK);
            a.show();
            txtGuideTele.requestFocus();
            return false;
        }
        return true;
    }

    private boolean ValidationPackage(){
        if(txtPackageName.getText().trim().isEmpty()){
            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Package Name Name..", ButtonType.OK);
            a.show();
            txtPackageName.requestFocus();
            return false;
        }else if(txtDuration.getText().trim().isEmpty()){
            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Duration..",ButtonType.OK);
            a.show();
            txtDuration.requestFocus();
            return false;
        }else if(txtPackagePrice.getText().trim().isEmpty()){
            Alert a=new Alert(Alert.AlertType.ERROR,"Empty text Package Price..",ButtonType.OK);
            a.show();
            txtPackagePrice.requestFocus();
            return false;
        }
        return true;
    }

    @FXML
    void onActionGuideName(ActionEvent event) {
        String guideName = txtGuideName.getText();
        boolean result = new Validation().validateName(guideName);
        if (result){
            txtGuideName.setStyle("-fx-background-color: white");
            txtGuideAddress.requestFocus();
        }else {
            txtGuideName.setStyle("-fx-background-color: red");
            txtGuideName.requestFocus();
        }
    }

    @FXML
    void onActionAddress(ActionEvent event) {
        String guideAddress = txtGuideName.getText();
        boolean result = new Validation().address(guideAddress);
        if (result){
            txtGuideAddress.setStyle("-fx-background-color: white");
            txtGuideTele.requestFocus();
        }else {
            txtGuideAddress.setStyle("-fx-background-color: red");
            txtGuideAddress.requestFocus();
        }
    }

    @FXML
    void onActionTelePhone(ActionEvent event) {
        boolean result = new Validation().validateContactNumber(txtGuideTele.getText());
        if (result){
            txtGuideTele.setStyle("-fx-background-color: white");
            btnAdd.requestFocus();
        }else{
            txtGuideTele.setStyle("-fx-background-color: red");
            txtGuideTele.requestFocus();
        }
    }

    @FXML
    void onActionPackageName(ActionEvent event) {
        String packageName = txtPackageName.getText();
        boolean result = new Validation().validateName(packageName);
        if (result){
            txtPackageName.setStyle("-fx-background-color: white");
//            comboGuideName.requestFocus();
        }else {
            txtPackageName.setStyle("-fx-background-color: red");
            txtPackageName.requestFocus();
        }
    }

    @FXML
    void onActionPackagePrice(ActionEvent event) {
        if (new Validation().onlyNumbers(txtPackagePrice.getText())){
            txtPackagePrice.setStyle("-fx-background-color: white");
//            btnCreatePackage.requestFocus();
        }else{
            txtPackagePrice.setStyle("-fx-background-color: red");
            txtPackagePrice.requestFocus();
        }
    }
}
