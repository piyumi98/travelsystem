package lk.ijse.travelsystem.controller;

import com.jfoenix.controls.JFXButton;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import lk.ijse.travelsystem.common.NotificationController;
import lk.ijse.travelsystem.common.PasswordUtill;
import lk.ijse.travelsystem.dao.CrudUtil;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class LoginVIewController implements Initializable {
    @FXML
    private TextField txtUserName;

    @FXML
    private PasswordField txtPassord;

    @FXML
    private JFXButton btnLogin;

    @FXML
    private JFXButton btnCancel;

    @FXML
    private JFXButton btnSignUp;


    @FXML
    private AnchorPane root;

    @FXML
    void btnCancelBtn(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Look, a Confirmation Dialog");
        alert.setContentText("Are you sure?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            System.exit(0);
        } else {
        }
    }

    @FXML
    void onActionLoginBtn(ActionEvent event) throws IOException{
        boolean result=false;

        ResultSet rst = null;
        try {
            rst = CrudUtil.executeQuery("Select * from user ");
            while (rst.next()){
                String userName=rst.getString(2);
                String salt=rst.getString(4);
                String secPw=rst.getString(3);


                boolean ifPasswordMatches=PasswordUtill.verifyUserPassword(txtPassord.getText(), salt, secPw);
                if (ifPasswordMatches && txtUserName.getText().equals(userName)) {

                    Parent parentRoot = FXMLLoader.load(this.getClass().getResource("/lk/ijse/travelsystem/view/homeView.fxml"));
                    Scene scene = new Scene(parentRoot);

                    Stage window = (Stage) this.root.getScene().getWindow();
                    window.setScene(scene);
                    window.centerOnScreen();

                    TranslateTransition tran = new TranslateTransition(Duration.millis(1000), scene.getRoot());
                    tran.setToX(0);
                    tran.setFromX(-scene.getWidth());
                    tran.play();
                    result=true;
                }
            }
            if (!result){
                NotificationController.createError("Error","Password  or Username error");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }


    }
    @FXML
    void onActionSignUp(ActionEvent event) throws IOException {
        Parent parentRoot = FXMLLoader.load(this.getClass().getResource("/lk/ijse/travelsystem/view/newUser.fxml"));
        Scene scene = new Scene(parentRoot);

        Stage window = (Stage) this.root.getScene().getWindow();
        window.setScene(scene);
        window.centerOnScreen();

        TranslateTransition tran = new TranslateTransition(Duration.millis(1000), scene.getRoot());
        tran.setToX(0);
        tran.setFromX(-scene.getWidth());
        tran.play();
    }


    @FXML
    void onActionUserName(ActionEvent event) {
        txtPassord.requestFocus();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

}
