package lk.ijse.travelsystem.bo;


import lk.ijse.travelsystem.bo.custom.impl.*;

public class BOFactory {
    private static BOFactory boFactory;

    private BOFactory() {
    }

    public static BOFactory getInstance() {
        if (boFactory == null) {
            boFactory = new BOFactory();
        }
        return boFactory;
    }

    public enum BOTyepes {
        TRAVELER,GUIDE,HOTEL,GUIDEDETAIL,HOTELDETAIL,BOOKING,LANGUAGES,LOCATIONDETAIL,MAINLOCATION,SUBLOCATION,PACKAGE,PAYMENT;
    }

    public SuperBO getBO(BOTyepes types) {
        switch (types) {
            case TRAVELER:
                return new TravelerBOImpl();
            case GUIDE:
                return new GuideBOImpl();
            case HOTEL:
                return new HotelBOImpl();
            case GUIDEDETAIL:
                return new GuideDetailBOImpl();
            case HOTELDETAIL:
                return new HotelDetailBOImpl();
            case BOOKING:
                return new BookingBOImpl();
            case LANGUAGES:
                return new LanguagesBOImpl();
            case LOCATIONDETAIL:
                return new LocationDetailBOImpl();
            case MAINLOCATION:
                return new MainLocationBOImpl();
            case SUBLOCATION:
                return new SubLocationBOImpl();
            case PACKAGE:
                return new PackageBOImpl();
            case PAYMENT:
                return new PaymentBOImpl();
            default:
                return null;
        }
    }
}
