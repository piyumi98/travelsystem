package lk.ijse.travelsystem.bo.custom;


import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.SubLocationDTO;

import java.util.ArrayList;

public interface SubLocationBO extends SuperBO {
    public boolean addSubLocation(SubLocationDTO locationDTO) throws Exception;
    public ArrayList<SubLocationDTO> getAllMainLocation()throws Exception;
}
