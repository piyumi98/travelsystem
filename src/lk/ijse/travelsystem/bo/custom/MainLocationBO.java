package lk.ijse.travelsystem.bo.custom;


import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.MainLocationDTO;

import java.util.ArrayList;

public interface MainLocationBO extends SuperBO {
    public boolean addMainLocation(MainLocationDTO locationDTO) throws Exception;
    public ArrayList<MainLocationDTO> getAllMainLocation()throws Exception;
}
