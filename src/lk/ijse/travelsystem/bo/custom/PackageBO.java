package lk.ijse.travelsystem.bo.custom;

import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.CommonDTO;
import lk.ijse.travelsystem.model.PackageDTO;

import java.util.ArrayList;

public interface PackageBO extends SuperBO {
    public boolean addPackage(CommonDTO dto) throws Exception;
    public ArrayList<PackageDTO> getAllPackage()throws Exception;
}
