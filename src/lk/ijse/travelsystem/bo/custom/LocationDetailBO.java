package lk.ijse.travelsystem.bo.custom;


import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.LocationDetailDTO;

public interface LocationDetailBO extends SuperBO {
    public boolean addLocationDetail(LocationDetailDTO locationDTO) throws Exception;
}
