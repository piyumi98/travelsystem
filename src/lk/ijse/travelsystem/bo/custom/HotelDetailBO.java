package lk.ijse.travelsystem.bo.custom;


import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.HotelDetailDTO;

import java.util.ArrayList;

public interface HotelDetailBO extends SuperBO {
    public boolean addHotelDetails(HotelDetailDTO locationDTO) throws Exception;
    public ArrayList<HotelDetailDTO> getAllHotelDetails()throws Exception;
}
