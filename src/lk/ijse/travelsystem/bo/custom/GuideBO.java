package lk.ijse.travelsystem.bo.custom;



import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.GuideDTO;

import java.util.ArrayList;

public interface GuideBO extends SuperBO {
    public boolean addGuide(GuideDTO locationDTO) throws Exception;
    public boolean updateGuide(GuideDTO locationDTO) throws Exception;
    public boolean deleteGuide(String name) throws Exception;
    public GuideDTO searchGuide(String name)throws Exception;
    public ArrayList<GuideDTO> getAllGuides()throws Exception;
}
