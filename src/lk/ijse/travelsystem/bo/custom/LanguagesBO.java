package lk.ijse.travelsystem.bo.custom;


import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.LangaugesDTO;

import java.util.ArrayList;

public interface LanguagesBO extends SuperBO {
    public ArrayList<LangaugesDTO> getAllMainLocation()throws Exception;
}
