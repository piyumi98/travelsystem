package lk.ijse.travelsystem.bo.custom;


import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.GuideDetailsDTO;

import java.util.ArrayList;

public interface GuideDetailBO extends SuperBO {
    public boolean addGuideDetails(GuideDetailsDTO locationDTO) throws Exception;
    public ArrayList<GuideDetailsDTO> getAllGuideDetails()throws Exception;
}
