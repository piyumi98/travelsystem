package lk.ijse.travelsystem.bo.custom;

import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.entity.Booking;
import lk.ijse.travelsystem.model.BookingDTO;
import lk.ijse.travelsystem.model.CommonBookingDTO;

import java.util.ArrayList;


public interface BookingBO extends SuperBO {
    public boolean addBooking(CommonBookingDTO bookingDTO) throws Exception;

    public boolean updateBooking(BookingDTO bookingDTO) throws Exception;

    public boolean deleteBooking(String bookingId) throws Exception;

    public ArrayList<BookingDTO> getAllBookings()throws Exception;
}
