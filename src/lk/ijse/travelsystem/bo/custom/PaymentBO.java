package lk.ijse.travelsystem.bo.custom;


import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.PaymentDTO;

import java.util.ArrayList;

public interface PaymentBO extends SuperBO {
    public boolean addPayment(PaymentDTO locationDTO) throws Exception;
    public ArrayList<PaymentDTO> getAllPayment()throws Exception;
}
