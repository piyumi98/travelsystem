package lk.ijse.travelsystem.bo.custom;


import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.TravelerDTO;

import java.util.ArrayList;

public interface TravelerBO extends SuperBO {

    public boolean addTraveler(TravelerDTO travelerDTO) throws Exception;
    public TravelerDTO searchTraveler(String name)throws Exception;
    public boolean deleteTraveler(String travelid)throws Exception;
    public boolean updateTraveler(TravelerDTO travelerDTO) throws Exception;
    public ArrayList<TravelerDTO> getAllTravelers()throws Exception;


}
