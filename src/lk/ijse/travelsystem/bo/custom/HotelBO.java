package lk.ijse.travelsystem.bo.custom;


import lk.ijse.travelsystem.bo.SuperBO;
import lk.ijse.travelsystem.model.HotelDTO;

import java.util.ArrayList;

public interface HotelBO extends SuperBO {
    public boolean addHotel(HotelDTO locationDTO) throws Exception;
    public boolean updateHotel(HotelDTO locationDTO) throws Exception;
    public boolean deleteHotel(String name) throws Exception;
    public ArrayList<HotelDTO> getAllHotels()throws Exception;
    public ArrayList<HotelDTO> getAllHotelsForLocation(String Location)throws Exception;
}
