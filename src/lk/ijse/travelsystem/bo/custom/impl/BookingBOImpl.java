package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.BookingBO;
import lk.ijse.travelsystem.dao.custom.BookingDAO;
import lk.ijse.travelsystem.dao.custom.PaymentDAO;
import lk.ijse.travelsystem.dao.custom.impl.BookingDAOImpl;
import lk.ijse.travelsystem.dao.custom.impl.PaymentDAOImpl;
import lk.ijse.travelsystem.db.DBConnection;
import lk.ijse.travelsystem.entity.Booking;
import lk.ijse.travelsystem.entity.Payment;
import lk.ijse.travelsystem.model.BookingDTO;
import lk.ijse.travelsystem.model.CommonBookingDTO;

import java.sql.Connection;
import java.util.ArrayList;

public class BookingBOImpl implements BookingBO {

    private static PaymentDAO paymentDAO=new PaymentDAOImpl();
    private static BookingDAO bookingDAO=new BookingDAOImpl();
    @Override
    public boolean addBooking(CommonBookingDTO bookingDTO) throws Exception {
        Connection connection= DBConnection.getInstance().getConnection();
        connection.setAutoCommit(false);
        boolean result=false;
        try{
            Payment payment=new Payment(bookingDTO.getPaymentDTO().getPaymentID(),bookingDTO.getPaymentDTO().getPaymentAmount()
                    ,bookingDTO.getPaymentDTO().getPaymentDate());
            Booking booking=new Booking(bookingDTO.getBookingDTO().getBookingID(),bookingDTO.getBookingDTO().getTravelID(),
                    bookingDTO.getBookingDTO().getPaymentid(),bookingDTO.getBookingDTO().getPackageName(),
                    bookingDTO.getBookingDTO().getBookingDate(),bookingDTO.getBookingDTO().getBookingPersonCount());
            result=paymentDAO.add(payment);
            if (!result){
                return false;
            }
            result=bookingDAO.add(booking);
            if (!result){
                connection.rollback();
                return false;
            }
            connection.commit();
            return true;
        }finally {
            connection.setAutoCommit(true);
        }
    }

    @Override
    public boolean updateBooking(BookingDTO b) throws Exception {
        return bookingDAO.update(new Booking(b.getBookingID(),b.getTravelID(),b.getPaymentid(),b.getPackageName(),b.getBookingDate(),b.getBookingPersonCount()));
    }

    @Override
    public boolean deleteBooking(String bookingId) throws Exception {
        return bookingDAO.delete(bookingId);
    }

    @Override
    public ArrayList<BookingDTO> getAllBookings() throws Exception {
        ArrayList<Booking> bookings=bookingDAO.getAll();
        ArrayList<BookingDTO>dtos=new ArrayList<>();
        for(Booking b: bookings){
            dtos.add(new BookingDTO(b.getBookingID(),b.getTravelID(),b.getPaymentid(),b.getPackageName(),b.getBookingDate(),b.getBookingPersonCount()));
        }
        return dtos;
    }
}
