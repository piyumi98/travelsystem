package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.LocationDetailBO;
import lk.ijse.travelsystem.dao.custom.LocationDetailDAO;
import lk.ijse.travelsystem.dao.custom.impl.LocationDetailDAOImpl;
import lk.ijse.travelsystem.entity.LocationDetail;
import lk.ijse.travelsystem.model.LocationDetailDTO;

public class LocationDetailBOImpl implements LocationDetailBO {

    private static LocationDetailDAO detailDAO=new LocationDetailDAOImpl();

    @Override
    public boolean addLocationDetail(LocationDetailDTO locationDTO) throws Exception {
        return detailDAO.add(new LocationDetail(
                locationDTO.getMainLocationName(),
                locationDTO.getSublocationName()
        ));
    }
}
