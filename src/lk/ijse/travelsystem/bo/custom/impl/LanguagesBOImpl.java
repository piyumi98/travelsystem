package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.LanguagesBO;
import lk.ijse.travelsystem.dao.custom.LanguagesDAO;
import lk.ijse.travelsystem.dao.custom.impl.LaguagesDAOImpl;
import lk.ijse.travelsystem.entity.Langauges;
import lk.ijse.travelsystem.model.LangaugesDTO;

import java.util.ArrayList;

public class LanguagesBOImpl implements LanguagesBO
{
    private static LanguagesDAO languagesDAO=new LaguagesDAOImpl();
    @Override
    public ArrayList<LangaugesDTO> getAllMainLocation() throws Exception {
        ArrayList<Langauges>location=languagesDAO.getAll();
        ArrayList<LangaugesDTO>dtos=new ArrayList<>();
        for(Langauges location1: location){
            dtos.add(new LangaugesDTO(location1.getLangName()));
        }
        return dtos;
    }
}
