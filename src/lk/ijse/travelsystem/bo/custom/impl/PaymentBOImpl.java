package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.PaymentBO;
import lk.ijse.travelsystem.dao.custom.PaymentDAO;
import lk.ijse.travelsystem.dao.custom.impl.PaymentDAOImpl;
import lk.ijse.travelsystem.entity.Payment;
import lk.ijse.travelsystem.model.PaymentDTO;

import java.util.ArrayList;

public class PaymentBOImpl implements PaymentBO {
    private static PaymentDAO paymentDAO=new PaymentDAOImpl();
    @Override
    public boolean addPayment(PaymentDTO locationDTO) throws Exception {
        return false;
    }

    @Override
    public ArrayList<PaymentDTO> getAllPayment() throws Exception {
        ArrayList<Payment> payments=paymentDAO.getAll();
        ArrayList<PaymentDTO>dtos=new ArrayList<>();
        for(Payment payment: payments){
            dtos.add(new PaymentDTO(payment.getPaymentID(),payment.getPaymentAmount(),payment.getPaymentDate()));
        }
        return dtos;
    }
}
