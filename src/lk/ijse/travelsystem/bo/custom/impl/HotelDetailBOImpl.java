package lk.ijse.travelsystem.bo.custom.impl;



import lk.ijse.travelsystem.bo.custom.HotelDetailBO;
import lk.ijse.travelsystem.dao.custom.HotelDetailDAO;
import lk.ijse.travelsystem.dao.custom.impl.HotelDetailDAOImpl;
import lk.ijse.travelsystem.entity.HotelDetail;
import lk.ijse.travelsystem.model.HotelDetailDTO;

import java.util.ArrayList;

public class HotelDetailBOImpl implements HotelDetailBO {
    private static HotelDetailDAO hotelDAO=new HotelDetailDAOImpl();

    @Override
    public boolean addHotelDetails(HotelDetailDTO locationDTO) throws Exception {
        return hotelDAO.add(new HotelDetail(locationDTO.getHotelID(),locationDTO.getPackageName()));
    }

    @Override
    public ArrayList<HotelDetailDTO> getAllHotelDetails() throws Exception {
        ArrayList<HotelDetail>location=hotelDAO.getAll();
        ArrayList<HotelDetailDTO>dtos=new ArrayList<>();
        for(HotelDetail location1: location){
            dtos.add(new HotelDetailDTO(location1.getHotelID(),location1.getPackageName()));
        }
        return dtos;
    }
}
