package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.GuideDetailBO;
import lk.ijse.travelsystem.dao.custom.GuideDetailDAO;
import lk.ijse.travelsystem.dao.custom.impl.GuideDetailDAOImpl;
import lk.ijse.travelsystem.entity.GuideDetails;
import lk.ijse.travelsystem.model.GuideDetailsDTO;

import java.util.ArrayList;

public class GuideDetailBOImpl implements GuideDetailBO {
    private static GuideDetailDAO detailDAO=new GuideDetailDAOImpl();

    @Override
    public boolean addGuideDetails(GuideDetailsDTO locationDTO) throws Exception {
        return detailDAO.add(new GuideDetails(locationDTO.getGuideID(),locationDTO.getLangName()));
    }

    @Override
    public ArrayList<GuideDetailsDTO> getAllGuideDetails() throws Exception {
        ArrayList<GuideDetails> location=detailDAO.getAll();
        ArrayList<GuideDetailsDTO>dtos=new ArrayList<>();
        for(GuideDetails location1: location){
            dtos.add(new GuideDetailsDTO(location1.getGuideID(),location1.getLangName()));
        }
        return dtos;
    }
}
