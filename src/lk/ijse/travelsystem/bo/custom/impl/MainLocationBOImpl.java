package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.MainLocationBO;
import lk.ijse.travelsystem.dao.custom.MainLocationDAO;
import lk.ijse.travelsystem.dao.custom.impl.MainLocationDAOImpl;
import lk.ijse.travelsystem.entity.MainLocation;
import lk.ijse.travelsystem.model.MainLocationDTO;

import java.util.ArrayList;

public class MainLocationBOImpl implements MainLocationBO {

    private static MainLocationDAO locationDAO=new MainLocationDAOImpl();

    @Override
    public boolean addMainLocation(MainLocationDTO locationDTO) throws Exception {
        return locationDAO.add(new MainLocation(
                locationDTO.getMainLocationName(),
                locationDTO.getLocationDescption(),
                locationDTO.getLocationDIstance(),
                locationDTO.getAvgTimeTOGOhours()
        ));
    }

    @Override
    public ArrayList<MainLocationDTO> getAllMainLocation() throws Exception {
        ArrayList<MainLocation>location=locationDAO.getAll();
        ArrayList<MainLocationDTO>dtos=new ArrayList<>();
        for(MainLocation location1: location){
            dtos.add(new MainLocationDTO(location1.getMainLocationName(),location1.getLocationDescption(),location1.getLocationDIstance(),location1.getAvgTimeTOGOhours()));
        }
        return dtos;
    }
}
