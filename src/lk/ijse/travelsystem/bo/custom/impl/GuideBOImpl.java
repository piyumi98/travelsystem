package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.GuideBO;
import lk.ijse.travelsystem.dao.custom.GuideDAO;
import lk.ijse.travelsystem.dao.custom.impl.GuideDAOImpl;
import lk.ijse.travelsystem.entity.Guide;
import lk.ijse.travelsystem.model.GuideDTO;

import java.util.ArrayList;

public class GuideBOImpl implements GuideBO {

    private static GuideDAO guideDAO=new GuideDAOImpl();

    @Override
    public boolean addGuide(GuideDTO locationDTO) throws Exception {
        return guideDAO.add(new Guide(locationDTO.getGuideID(),locationDTO.getGuideName(),
                locationDTO.getGuideAddress(),locationDTO.getGuideTelNo()));
    }

    @Override
    public boolean updateGuide(GuideDTO locationDTO) throws Exception {
        return guideDAO.update(new Guide(locationDTO.getGuideID(),locationDTO.getGuideName(),
                locationDTO.getGuideAddress(),locationDTO.getGuideTelNo()));
    }

    @Override
    public boolean deleteGuide(String name) throws Exception {
        return guideDAO.delete(name);
    }

    @Override
    public GuideDTO searchGuide(String name) throws Exception {
        Guide guide=guideDAO.search(name);
        return new GuideDTO(guide.getGuideID(),guide.getGuideName(),guide.getGuideAddress(),guide.getGuideTelNo());
    }

    @Override
    public ArrayList<GuideDTO> getAllGuides() throws Exception {
        ArrayList<Guide> location=guideDAO.getAll();
        ArrayList<GuideDTO>dtos=new ArrayList<>();
        for(Guide location1: location){
            dtos.add(new GuideDTO(location1.getGuideID(),location1.getGuideName(),location1.getGuideAddress(),location1.getGuideTelNo()));
        }
        return dtos;
    }
}
