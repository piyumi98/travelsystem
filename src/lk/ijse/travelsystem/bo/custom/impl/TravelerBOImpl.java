package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.TravelerBO;
import lk.ijse.travelsystem.dao.DAOFactory;
import lk.ijse.travelsystem.dao.custom.TravelerDAO;
import lk.ijse.travelsystem.entity.Traveler;
import lk.ijse.travelsystem.model.TravelerDTO;

import java.util.ArrayList;

public class TravelerBOImpl implements TravelerBO {
    private static TravelerDAO travelerDAO;

    public TravelerBOImpl() {
        travelerDAO= DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.TRAVELER);
    }

    @Override
    public boolean addTraveler(TravelerDTO travelerDTO) throws Exception {
        return travelerDAO.add(new Traveler(
                travelerDTO.getTravelID(),
                travelerDTO.getTravelerFullName(),
                travelerDTO.getTravelerCountry(),
                travelerDTO.getTravelerMobileNumber(),
                travelerDTO.getTravelerEmail()
        ));
    }

    @Override
    public TravelerDTO searchTraveler(String name) throws Exception {
        Traveler traveler=travelerDAO.search(name);
        return new TravelerDTO(traveler.getTravelID(),traveler.getTravelerFullName(),traveler.getTravelerCountry(),
                traveler.getTravelerMobileNumber(),traveler.getTravelerEmail());
    }

    @Override
    public boolean deleteTraveler(String travelid) throws Exception {
        return travelerDAO.delete(travelid);
    }

    @Override
    public boolean updateTraveler(TravelerDTO travelerDTO) throws Exception {
        return travelerDAO.update(new Traveler(
                travelerDTO.getTravelID(),
                travelerDTO.getTravelerFullName(),
                travelerDTO.getTravelerCountry(),
                travelerDTO.getTravelerMobileNumber(),
                travelerDTO.getTravelerEmail()
        ));
    }

    @Override
    public ArrayList<TravelerDTO> getAllTravelers() throws Exception {
        ArrayList<Traveler> all = travelerDAO.getAll();
        ArrayList<TravelerDTO> travelerDTOS=new ArrayList<>();
        for (Traveler traveler:all) {
            travelerDTOS.add(new TravelerDTO(
                    traveler.getTravelID(),
                    traveler.getTravelerFullName(),
                    traveler.getTravelerCountry(),
                    traveler.getTravelerMobileNumber(),
                    traveler.getTravelerEmail()
            ));
        }
        return travelerDTOS;
    }
}
