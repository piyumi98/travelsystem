package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.PackageBO;
import lk.ijse.travelsystem.dao.custom.HotelDetailDAO;
import lk.ijse.travelsystem.dao.custom.PackageDAO;
import lk.ijse.travelsystem.dao.custom.impl.HotelDetailDAOImpl;
import lk.ijse.travelsystem.dao.custom.impl.PackageDAOImpl;
import lk.ijse.travelsystem.db.DBConnection;
import lk.ijse.travelsystem.entity.HotelDetail;
import lk.ijse.travelsystem.entity.Package;
import lk.ijse.travelsystem.model.CommonDTO;
import lk.ijse.travelsystem.model.PackageDTO;

import java.sql.Connection;
import java.util.ArrayList;

public class PackageBOImpl implements PackageBO {

    private static PackageDAO packageDAO=new PackageDAOImpl();
    private static HotelDetailDAO detailDAO=new HotelDetailDAOImpl();
    @Override
    public boolean addPackage(CommonDTO dto) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        connection.setAutoCommit(false);
        boolean result;
        try{
            Package aPackage=new Package(dto.getPackageDTO().getPackageName(),dto.getPackageDTO().getGuideID(),
                    dto.getPackageDTO().getTourBeginDate(),dto.getPackageDTO().getTourEndDate(),dto.getPackageDTO().getTourDuration(),
                    dto.getPackageDTO().getPackagePrice());
            HotelDetail detail=new HotelDetail(dto.getDetailDTO().getHotelID(),dto.getDetailDTO().getPackageName());
            result=packageDAO.add(aPackage);
            if (!result){
                return false;
            }
            result=detailDAO.add(detail);
            if (!result){
                connection.rollback();
                return false;
            }
            connection.commit();
            return true;
        }finally {
            connection.setAutoCommit(true);
        }
    }

    @Override
    public ArrayList<PackageDTO> getAllPackage() throws Exception {
        ArrayList<Package> location=packageDAO.getAll();
        ArrayList<PackageDTO>dtos=new ArrayList<>();
        for(Package location1: location){
            dtos.add(new PackageDTO(location1.getPackageName(),location1.getGuideID(),location1.getTourBeginDate(),
                    location1.getTourEndDate(),location1.getTourDuration(),location1.getPackagePrice()));
        }
        return dtos;
    }
}
