package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.HotelBO;
import lk.ijse.travelsystem.dao.custom.HotelDAO;
import lk.ijse.travelsystem.dao.custom.impl.HotelDAOImpl;
import lk.ijse.travelsystem.entity.Hotel;
import lk.ijse.travelsystem.model.HotelDTO;

import java.util.ArrayList;

public class HotelBOImpl implements HotelBO {

    private static HotelDAO hotelDAO=new HotelDAOImpl();

    @Override
    public boolean addHotel(HotelDTO locationDTO) throws Exception {
        return hotelDAO.add(new Hotel(
                locationDTO.getHotelID(),
                locationDTO.getMainLocationName(),
                locationDTO.getHotelName(),
                locationDTO.getHotelAddress(),
                locationDTO.getHotelLocation(),
                locationDTO.getHotelTel()
        ));
    }

    @Override
    public boolean updateHotel(HotelDTO locationDTO) throws Exception {
        return hotelDAO.update(new Hotel(
                locationDTO.getHotelID(),
                locationDTO.getMainLocationName(),
                locationDTO.getHotelName(),
                locationDTO.getHotelAddress(),
                locationDTO.getHotelLocation(),
                locationDTO.getHotelTel()
        ));
    }

    @Override
    public boolean deleteHotel(String name) throws Exception {
        return hotelDAO.delete(name);
    }

    @Override
    public ArrayList<HotelDTO> getAllHotels() throws Exception {
        ArrayList<Hotel> location=hotelDAO.getAll();
        ArrayList<HotelDTO>dtos=new ArrayList<>();
        for(Hotel location1: location){
            dtos.add(new HotelDTO(location1.getHotelID(),location1.getMainLocationName(),location1.getHotelName(),
                    location1.getHotelAddress(),location1.getHotelLocation(),location1.getHotelTel()));
        }
        return dtos;
    }

    @Override
    public ArrayList<HotelDTO> getAllHotelsForLocation(String Location) throws Exception {
        ArrayList<Hotel>location=hotelDAO.getAllHotelsForLocation(Location);
        ArrayList<HotelDTO>dtos=new ArrayList<>();
        for(Hotel location1: location){
            dtos.add(new HotelDTO(location1.getHotelID(),location1.getMainLocationName(),location1.getHotelName(),
                    location1.getHotelAddress(),location1.getHotelLocation(),location1.getHotelTel()));
        }
        return dtos;
    }
}
