package lk.ijse.travelsystem.bo.custom.impl;


import lk.ijse.travelsystem.bo.custom.SubLocationBO;
import lk.ijse.travelsystem.dao.custom.SubLocationDAO;
import lk.ijse.travelsystem.dao.custom.impl.SubLocationDAOImpl;
import lk.ijse.travelsystem.entity.SubLocation;
import lk.ijse.travelsystem.model.SubLocationDTO;

import java.util.ArrayList;

public class SubLocationBOImpl implements SubLocationBO {
    private static SubLocationDAO locationDAO=new SubLocationDAOImpl();

    @Override
    public boolean addSubLocation(SubLocationDTO locationDTO) throws Exception {
        return locationDAO.add(new SubLocation(
                locationDTO.getSublocationName(),
                locationDTO.getLocationDIstance()
        ));
    }

    @Override
    public ArrayList<SubLocationDTO> getAllMainLocation() throws Exception {
        ArrayList<SubLocation>location=locationDAO.getAll();
        ArrayList<SubLocationDTO> dtos=new ArrayList<>();
        for(SubLocation location1: location){
            dtos.add(new SubLocationDTO(location1.getSublocationName(),location1.getLocationDIstance()));
        }
        return dtos;
    }
}
