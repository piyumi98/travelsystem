package lk.ijse.travelsystem.entity;

public class GuideDetails {
    private int guideID;
    private String langName;

    public GuideDetails() {
    }

    public GuideDetails(int guideID, String langName) {
        this.guideID = guideID;
        this.langName = langName;
    }

    public int getGuideID() {
        return guideID;
    }

    public void setGuideID(int guideID) {
        this.guideID = guideID;
    }

    public String getLangName() {
        return langName;
    }

    public void setLangName(String langName) {
        this.langName = langName;
    }
}
