package lk.ijse.travelsystem.entity;

public class Guide {
    private int guideID;
    private String guideName;
    private String guideAddress;
    private String guideTelNo;

    public Guide() {
    }

    public Guide(int guideID, String guideName, String guideAddress, String guideTelNo) {
        this.guideID = guideID;
        this.guideName = guideName;
        this.guideAddress = guideAddress;
        this.guideTelNo = guideTelNo;
    }

    public int getGuideID() {
        return guideID;
    }

    public void setGuideID(int guideID) {
        this.guideID = guideID;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public String getGuideAddress() {
        return guideAddress;
    }

    public void setGuideAddress(String guideAddress) {
        this.guideAddress = guideAddress;
    }

    public String getGuideTelNo() {
        return guideTelNo;
    }

    public void setGuideTelNo(String guideTelNo) {
        this.guideTelNo = guideTelNo;
    }
}
