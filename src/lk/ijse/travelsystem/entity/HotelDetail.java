package lk.ijse.travelsystem.entity;

public class HotelDetail {
    private int hotelID;
    private String packageName;

    public HotelDetail() {
    }

    public HotelDetail(int hotelID, String packageName) {
        this.hotelID = hotelID;
        this.packageName = packageName;
    }

    public int getHotelID() {
        return hotelID;
    }

    public void setHotelID(int hotelID) {
        this.hotelID = hotelID;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
