package lk.ijse.travelsystem.entity;

public class Langauges {
    private String langName;

    public Langauges() {
    }

    public Langauges(String langName) {
        this.langName = langName;
    }

    public String getLangName() {
        return langName;
    }

    public void setLangName(String langName) {
        this.langName = langName;
    }
}
