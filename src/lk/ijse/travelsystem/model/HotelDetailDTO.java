package lk.ijse.travelsystem.model;

public class HotelDetailDTO {
    private int hotelID;
    private String packageName;

    public HotelDetailDTO() {
    }

    public HotelDetailDTO(int hotelID, String packageName) {
        this.hotelID = hotelID;
        this.packageName = packageName;
    }

    public int getHotelID() {
        return hotelID;
    }

    public void setHotelID(int hotelID) {
        this.hotelID = hotelID;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @Override
    public String toString() {
        return "HotelDetailDTO{" +
                "hotelID=" + hotelID +
                ", packageName='" + packageName + '\'' +
                '}';
    }
}
