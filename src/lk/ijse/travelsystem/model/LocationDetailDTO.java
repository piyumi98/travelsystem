package lk.ijse.travelsystem.model;

public class LocationDetailDTO {
    private String mainLocationName;
    private String sublocationName;

    public LocationDetailDTO() {
    }

    public LocationDetailDTO(String mainLocationName, String sublocationName) {
        this.mainLocationName = mainLocationName;
        this.sublocationName = sublocationName;
    }

    public String getMainLocationName() {
        return mainLocationName;
    }

    public void setMainLocationName(String mainLocationName) {
        this.mainLocationName = mainLocationName;
    }

    public String getSublocationName() {
        return sublocationName;
    }

    public void setSublocationName(String sublocationName) {
        this.sublocationName = sublocationName;
    }
}
