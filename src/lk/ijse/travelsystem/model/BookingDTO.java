package lk.ijse.travelsystem.model;

import java.util.Date;

public class BookingDTO {
    private String bookingID;
    private String travelID;
    private String paymentid;
    private String packageName;
    private String bookingDate;
    private int bookingPersonCount;

    public BookingDTO() {
    }

    public BookingDTO(String bookingID, String travelID, String paymentid, String packageName, String bookingDate, int bookingPersonCount) {
        this.bookingID = bookingID;
        this.travelID = travelID;
        this.paymentid = paymentid;
        this.packageName = packageName;
        this.bookingDate = bookingDate;
        this.bookingPersonCount = bookingPersonCount;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public String getTravelID() {
        return travelID;
    }

    public void setTravelID(String travelID) {
        this.travelID = travelID;
    }

    public String getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(String paymentid) {
        this.paymentid = paymentid;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public int getBookingPersonCount() {
        return bookingPersonCount;
    }

    public void setBookingPersonCount(int bookingPersonCount) {
        this.bookingPersonCount = bookingPersonCount;
    }
}
