package lk.ijse.travelsystem.model;

public class HotelDTO {
    private int hotelID;
    private String mainLocationName;
    private String hotelName;
    private String hotelAddress;
    private String hotelLocation;
    private String hotelTel;

    public HotelDTO() {
    }

    public HotelDTO(int hotelID, String mainLocationName, String hotelName, String hotelAddress, String hotelLocation, String hotelTel) {
        this.hotelID = hotelID;
        this.mainLocationName = mainLocationName;
        this.hotelName = hotelName;
        this.hotelAddress = hotelAddress;
        this.hotelLocation = hotelLocation;
        this.hotelTel = hotelTel;
    }

    public int getHotelID() {
        return hotelID;
    }

    public void setHotelID(int hotelID) {
        this.hotelID = hotelID;
    }

    public String getMainLocationName() {
        return mainLocationName;
    }

    public void setMainLocationName(String mainLocationName) {
        this.mainLocationName = mainLocationName;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public String getHotelLocation() {
        return hotelLocation;
    }

    public void setHotelLocation(String hotelLocation) {
        this.hotelLocation = hotelLocation;
    }

    public String getHotelTel() {
        return hotelTel;
    }

    public void setHotelTel(String hotelTel) {
        this.hotelTel = hotelTel;
    }

    @Override
    public String toString() {
        return  hotelName;
    }
}
