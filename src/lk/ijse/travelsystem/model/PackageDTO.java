package lk.ijse.travelsystem.model;

import java.util.Date;

public class PackageDTO {
    private String packageName;
    private int guideID;
    private String tourBeginDate;
    private String tourEndDate;
    private int tourDuration;
    private double packagePrice;

    public PackageDTO() {
    }

    public PackageDTO(String packageName, int guideID, String tourBeginDate, String tourEndDate, int tourDuration, double packagePrice) {
        this.packageName = packageName;
        this.guideID = guideID;
        this.tourBeginDate = tourBeginDate;
        this.tourEndDate = tourEndDate;
        this.tourDuration = tourDuration;
        this.packagePrice = packagePrice;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getGuideID() {
        return guideID;
    }

    public void setGuideID(int guideID) {
        this.guideID = guideID;
    }

    public String getTourBeginDate() {
        return tourBeginDate;
    }

    public void setTourBeginDate(String tourBeginDate) {
        this.tourBeginDate = tourBeginDate;
    }

    public String getTourEndDate() {
        return tourEndDate;
    }

    public void setTourEndDate(String tourEndDate) {
        this.tourEndDate = tourEndDate;
    }

    public int getTourDuration() {
        return tourDuration;
    }

    public void setTourDuration(int tourDuration) {
        this.tourDuration = tourDuration;
    }

    public double getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(double packagePrice) {
        this.packagePrice = packagePrice;
    }
}
