package lk.ijse.travelsystem.model;

public class CommonDTO {
    private PackageDTO packageDTO;
    private HotelDetailDTO detailDTO;

    public CommonDTO() {
    }

    public CommonDTO(PackageDTO packageDTO, HotelDetailDTO detailDTO) {
        this.packageDTO = packageDTO;
        this.detailDTO = detailDTO;
    }

    public PackageDTO getPackageDTO() {
        return packageDTO;
    }

    public void setPackageDTO(PackageDTO packageDTO) {
        this.packageDTO = packageDTO;
    }

    public HotelDetailDTO getDetailDTO() {
        return detailDTO;
    }

    public void setDetailDTO(HotelDetailDTO detailDTO) {
        this.detailDTO = detailDTO;
    }

    @Override
    public String toString() {
        return "CommonDTO{" +
                "packageDTO=" + packageDTO +
                ", detailDTO=" + detailDTO +
                '}';
    }
}
