package lk.ijse.travelsystem.model;

public class CommonBookingDTO {
    private PaymentDTO paymentDTO;
    private BookingDTO bookingDTO;

    public CommonBookingDTO() {
    }

    public CommonBookingDTO(PaymentDTO paymentDTO, BookingDTO bookingDTO) {
        this.paymentDTO = paymentDTO;
        this.bookingDTO = bookingDTO;
    }

    public PaymentDTO getPaymentDTO() {
        return paymentDTO;
    }

    public void setPaymentDTO(PaymentDTO paymentDTO) {
        this.paymentDTO = paymentDTO;
    }

    public BookingDTO getBookingDTO() {
        return bookingDTO;
    }

    public void setBookingDTO(BookingDTO bookingDTO) {
        this.bookingDTO = bookingDTO;
    }
}
