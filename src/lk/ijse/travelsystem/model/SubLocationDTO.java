package lk.ijse.travelsystem.model;

public class SubLocationDTO {
    private String sublocationName;
    private double locationDIstance;

    public SubLocationDTO() {
    }

    public SubLocationDTO(String sublocationName, double locationDIstance) {
        this.sublocationName = sublocationName;
        this.locationDIstance = locationDIstance;
    }

    public String getSublocationName() {
        return sublocationName;
    }

    public void setSublocationName(String sublocationName) {
        this.sublocationName = sublocationName;
    }

    public double getLocationDIstance() {
        return locationDIstance;
    }

    public void setLocationDIstance(double locationDIstance) {
        this.locationDIstance = locationDIstance;
    }
}
