package lk.ijse.travelsystem.model;

import java.util.Date;

public class PaymentDTO {
    private String paymentID;
    private double paymentAmount;
    private String paymentDate;

    public PaymentDTO() {
    }

    public PaymentDTO(String paymentID, double paymentAmount, String paymentDate) {
        this.paymentID = paymentID;
        this.paymentAmount = paymentAmount;
        this.paymentDate = paymentDate;
    }

    public String getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(String paymentID) {
        this.paymentID = paymentID;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }
}
