package lk.ijse.travelsystem.model;

public class LangaugesDTO {
    private String langName;

    public LangaugesDTO() {
    }

    public LangaugesDTO(String langName) {
        this.langName = langName;
    }

    public String getLangName() {
        return langName;
    }

    public void setLangName(String langName) {
        this.langName = langName;
    }
}
