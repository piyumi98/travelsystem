package lk.ijse.travelsystem.model;

public class TravelerDTO {
    private String travelID;
    private String travelerFullName;
    private String travelerCountry;
    private String travelerMobileNumber;
    private String travelerEmail;

    public TravelerDTO() {
    }

    public TravelerDTO(String travelID, String travelerFullName, String travelerCountry, String travelerMobileNumber, String travelerEmail) {
        this.travelID = travelID;
        this.travelerFullName = travelerFullName;
        this.travelerCountry = travelerCountry;
        this.travelerMobileNumber = travelerMobileNumber;
        this.travelerEmail = travelerEmail;
    }

    public String getTravelID() {
        return travelID;
    }

    public void setTravelID(String travelID) {
        this.travelID = travelID;
    }

    public String getTravelerFullName() {
        return travelerFullName;
    }

    public void setTravelerFullName(String travelerFullName) {
        this.travelerFullName = travelerFullName;
    }

    public String getTravelerCountry() {
        return travelerCountry;
    }

    public void setTravelerCountry(String travelerCountry) {
        this.travelerCountry = travelerCountry;
    }

    public String getTravelerMobileNumber() {
        return travelerMobileNumber;
    }

    public void setTravelerMobileNumber(String travelerMobileNumber) {
        this.travelerMobileNumber = travelerMobileNumber;
    }

    public String getTravelerEmail() {
        return travelerEmail;
    }

    public void setTravelerEmail(String travelerEmail) {
        this.travelerEmail = travelerEmail;
    }
}
