package lk.ijse.travelsystem.model;

public class MainLocationDTO {
    private String mainLocationName;
    private String locationDescption;
    private double locationDIstance;
    private int avgTimeTOGOhours;

    public MainLocationDTO() {
    }

    public MainLocationDTO(String mainLocationName, String locationDescption, double locationDIstance, int avgTimeTOGOhours) {
        this.mainLocationName = mainLocationName;
        this.locationDescption = locationDescption;
        this.locationDIstance = locationDIstance;
        this.avgTimeTOGOhours = avgTimeTOGOhours;
    }

    public String getMainLocationName() {
        return mainLocationName;
    }

    public void setMainLocationName(String mainLocationName) {
        this.mainLocationName = mainLocationName;
    }

    public String getLocationDescption() {
        return locationDescption;
    }

    public void setLocationDescption(String locationDescption) {
        this.locationDescption = locationDescption;
    }

    public double getLocationDIstance() {
        return locationDIstance;
    }

    public void setLocationDIstance(double locationDIstance) {
        this.locationDIstance = locationDIstance;
    }

    public int getAvgTimeTOGOhours() {
        return avgTimeTOGOhours;
    }

    public void setAvgTimeTOGOhours(int avgTimeTOGOhours) {
        this.avgTimeTOGOhours = avgTimeTOGOhours;
    }
}
