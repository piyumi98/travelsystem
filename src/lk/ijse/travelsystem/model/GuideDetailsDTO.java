package lk.ijse.travelsystem.model;

public class GuideDetailsDTO {
    private int guideID;
    private String langName;

    public GuideDetailsDTO() {
    }

    public GuideDetailsDTO(int guideID, String langName) {
        this.guideID = guideID;
        this.langName = langName;
    }

    public int getGuideID() {
        return guideID;
    }

    public void setGuideID(int guideID) {
        this.guideID = guideID;
    }

    public String getLangName() {
        return langName;
    }

    public void setLangName(String langName) {
        this.langName = langName;
    }

    @Override
    public String toString() {
        return "GuideDetailsDTO{" +
                "guideID=" + guideID +
                ", langName='" + langName + '\'' +
                '}';
    }
}
