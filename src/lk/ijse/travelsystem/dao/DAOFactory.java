package lk.ijse.travelsystem.dao;


import lk.ijse.travelsystem.dao.custom.impl.*;

public class DAOFactory {
    private static DAOFactory daoFactory;

    private DAOFactory() {
    }

    public static DAOFactory getInstance() {
        if (daoFactory == null) {
            daoFactory = new DAOFactory();
        }
        return daoFactory;
    }

    public enum DAOTypes {
        TRAVELER,GUIDE,HOTEL,GUIDEDETAIL,HOTELDETAIL,BOOKING,LANGUAGES,LOCATIONDETAIL,MAINLOCATION,SUBLOCATION,PACKAGE,PAYMENT;
    }

    public <T extends SuperDAO> T getDAO(DAOTypes types) {
        switch (types) {
            case TRAVELER:
                return (T) new TravelerDAOImpl();
            case GUIDE:
                return (T) new GuideDAOImpl();
            case HOTEL:
                return (T) new HotelDAOImpl();
            case GUIDEDETAIL:
                return (T) new GuideDetailDAOImpl();
            case HOTELDETAIL:
                return (T) new HotelDetailDAOImpl();
            case BOOKING:
                return (T) new BookingDAOImpl();
            case LANGUAGES:
                return (T) new LaguagesDAOImpl();
            case LOCATIONDETAIL:
                return (T) new LocationDetailDAOImpl();
            case MAINLOCATION:
                return (T) new MainLocationDAOImpl();
            case SUBLOCATION:
                return (T) new SubLocationDAOImpl();
            case PACKAGE:
                return (T) new PackageDAOImpl();
            case PAYMENT:
                return (T) new PaymentDAOImpl();
            default:
                return null;

        }
    }
}
