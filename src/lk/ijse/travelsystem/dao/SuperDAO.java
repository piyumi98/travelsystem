package lk.ijse.travelsystem.dao;

import java.sql.SQLException;
import java.util.ArrayList;

public interface SuperDAO<T, ID> {
    public boolean add(T t) throws ClassNotFoundException, SQLException;

    public boolean update(T t) throws ClassNotFoundException, SQLException;

    public boolean delete(ID t) throws ClassNotFoundException, SQLException;

    public T search(ID t) throws ClassNotFoundException, SQLException;

    public ArrayList<T> getAll() throws ClassNotFoundException, SQLException;
}
