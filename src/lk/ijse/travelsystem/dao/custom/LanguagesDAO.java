package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.Langauges;

public interface LanguagesDAO extends SuperDAO<Langauges,String> {
}
