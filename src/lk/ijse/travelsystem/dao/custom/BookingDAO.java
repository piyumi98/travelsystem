package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.Booking;

public interface BookingDAO extends SuperDAO<Booking,String> {
}
