package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.Payment;

public interface PaymentDAO extends SuperDAO<Payment,String> {
}
