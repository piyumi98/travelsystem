package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.GuideDetails;

public interface GuideDetailDAO extends SuperDAO<GuideDetails,String> {
}
