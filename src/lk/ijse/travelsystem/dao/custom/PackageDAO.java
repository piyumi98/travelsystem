package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.Package;

public interface PackageDAO extends SuperDAO<Package,String> {
}
