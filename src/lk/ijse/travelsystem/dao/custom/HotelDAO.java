package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.Hotel;

import java.util.ArrayList;

public interface HotelDAO extends SuperDAO<Hotel,String> {
    public ArrayList<Hotel> getAllHotelsForLocation(String Location)throws Exception;
}
