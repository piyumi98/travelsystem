package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.Guide;

public interface GuideDAO extends SuperDAO<Guide,String> {
}
