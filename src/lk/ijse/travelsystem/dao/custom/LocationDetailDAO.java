package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.LocationDetail;

public interface LocationDetailDAO extends SuperDAO<LocationDetail,String> {
}
