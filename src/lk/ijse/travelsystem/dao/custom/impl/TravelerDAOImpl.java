package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.TravelerDAO;
import lk.ijse.travelsystem.entity.Traveler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TravelerDAOImpl implements TravelerDAO {

    @Override
    public boolean add(Traveler traveler) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into traveler values (?,?,?,?,?) ",
                traveler.getTravelID(),
                traveler.getTravelerFullName(),
                traveler.getTravelerCountry(),
                traveler.getTravelerMobileNumber(),
                traveler.getTravelerEmail()
        );
    }

    @Override
    public boolean update(Traveler traveler) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("update traveler set travelerFullName=?,travelerCountry=?," +
                        "travelerMobileNumber=?,travelerEmail=? where travelID=?  ",
                traveler.getTravelerFullName(),
                traveler.getTravelerCountry(),
                traveler.getTravelerMobileNumber(),
                traveler.getTravelerEmail(),
                traveler.getTravelID()
        );
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("delete from traveler where travelID=? ", t);
    }

    @Override
    public Traveler search(String t) throws ClassNotFoundException, SQLException {
        ResultSet rst=CrudUtil.executeQuery("select * from traveler where travelerFullName=? ", t);
        if(rst.next()){
            return new Traveler(rst.getString(1),rst.getString(2),rst.getString(3),
                    rst.getString(4),rst.getString(5));
        }else{
            return null;
        }
    }

    @Override
    public ArrayList<Traveler> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<Traveler> traveler=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From traveler");
        while(rst.next()){
            traveler.add(new Traveler(rst.getString(1),rst.getString(2),rst.getString(3),
                    rst.getString(4),rst.getString(5)));
        }
        return traveler;
    }
}
