package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.MainLocationDAO;
import lk.ijse.travelsystem.entity.MainLocation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MainLocationDAOImpl implements MainLocationDAO {
    @Override
    public boolean add(MainLocation mainLocation) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into mainlocation values (?,?,?,? ) ",
                mainLocation.getMainLocationName(),
                mainLocation.getLocationDescption(),
                mainLocation.getLocationDIstance(),
                mainLocation.getAvgTimeTOGOhours()
        );
    }

    @Override
    public boolean update(MainLocation mainLocation) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("update mainlocation set locationDescption=?,locationDIstance=?,avgTimeTOGOhours=? where mainLocationName=? ",
                mainLocation.getLocationDescption(),
                mainLocation.getLocationDIstance(),
                mainLocation.getAvgTimeTOGOhours(),
                mainLocation.getMainLocationName()
        );
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("delete from mainlocation where mainLocationName=? ", t);
    }

    @Override
    public MainLocation search(String t) throws ClassNotFoundException, SQLException {
        ResultSet rst=CrudUtil.executeQuery("select * from mainlocation where mainLocationName=? ", t);
        if(rst.next()){
            return new MainLocation(rst.getString(1),rst.getString(2),rst.getDouble(3),rst.getInt(4));
        }else{
            return null;
        }
    }

    @Override
    public ArrayList<MainLocation> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<MainLocation>locations=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From mainlocation");
        while(rst.next()){
            locations.add(new MainLocation(rst.getString(1),rst.getString(2),rst.getDouble(3),rst.getInt(4)));
        }
        return locations;
    }
}
