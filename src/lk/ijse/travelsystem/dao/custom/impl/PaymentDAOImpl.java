package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.PaymentDAO;
import lk.ijse.travelsystem.entity.Payment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PaymentDAOImpl implements PaymentDAO {
    @Override
    public boolean add(Payment payment) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into payment values (?,?,?) ",
                payment.getPaymentID(),
                payment.getPaymentAmount(),
                payment.getPaymentDate()
        );
    }

    @Override
    public boolean update(Payment payment) throws ClassNotFoundException, SQLException {
        return false;
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return false;
    }

    @Override
    public Payment search(String t) throws ClassNotFoundException, SQLException {
        return null;
    }

    @Override
    public ArrayList<Payment> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<Payment> payments=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From payment");
        while(rst.next()){
            payments.add(new Payment(rst.getString(1),rst.getDouble(2),rst.getString(3)));
        }
        return payments;
    }
}
