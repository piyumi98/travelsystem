package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.LocationDetailDAO;
import lk.ijse.travelsystem.entity.LocationDetail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LocationDetailDAOImpl implements LocationDetailDAO {
    @Override
    public boolean add(LocationDetail locationDetail) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into locationdetail values (?,? ) ",
                locationDetail.getMainLocationName(),
                locationDetail.getSublocationName()
        );
    }

    @Override
    public boolean update(LocationDetail locationDetail) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("update locationdetail set sublocationName=? where mainLocationName=? ",
                locationDetail.getSublocationName(),
                locationDetail.getMainLocationName()
        );
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("delete from locationdetail where mainLocationName=? ", t);
    }

    @Override
    public LocationDetail search(String t) throws ClassNotFoundException, SQLException {
        ResultSet rst=CrudUtil.executeQuery("select * from locationdetail where mainLocationName=? ", t);
        if(rst.next()){
            return new LocationDetail(rst.getString(1),rst.getString(2));
        }else{
            return null;
        }
    }

    @Override
    public ArrayList<LocationDetail> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<LocationDetail> locations=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From locationdetail");
        while(rst.next()){
            locations.add(new LocationDetail(rst.getString(1),rst.getString(2)));
        }
        return locations;
    }
}
