package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.LanguagesDAO;
import lk.ijse.travelsystem.entity.Langauges;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LaguagesDAOImpl implements LanguagesDAO {
    @Override
    public boolean add(Langauges langauges) throws ClassNotFoundException, SQLException {
        return false;
    }

    @Override
    public boolean update(Langauges langauges) throws ClassNotFoundException, SQLException {
        return false;
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return false;
    }

    @Override
    public Langauges search(String t) throws ClassNotFoundException, SQLException {
        return null;
    }

    @Override
    public ArrayList<Langauges> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<Langauges> traveler=new ArrayList<>();
        ResultSet rst= CrudUtil.executeQuery("select * From langauges");
        while(rst.next()){
            traveler.add(new Langauges(rst.getString(1)));
        }
        return traveler;
    }
}
