package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.HotelDAO;
import lk.ijse.travelsystem.entity.Hotel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HotelDAOImpl implements HotelDAO {
    @Override
    public boolean add(Hotel hotel) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into hotel values (null,?,?,?,?,?) ",
                hotel.getMainLocationName(),
                hotel.getHotelName(),
                hotel.getHotelAddress(),
                hotel.getHotelLocation(),
                hotel.getHotelTel()
        );
    }

    @Override
    public boolean update(Hotel hotel) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("update hotel set hotelAddress=?,hotelLocation=?," +
                        "hotelTel=? where hotelName=?  ",
                hotel.getHotelAddress(),
                hotel.getHotelLocation(),
                hotel.getHotelTel(),
                hotel.getHotelName()
        );
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("delete from hotel where hotelName=? ", t);
    }

    @Override
    public Hotel search(String t) throws ClassNotFoundException, SQLException {
        ResultSet rst=CrudUtil.executeQuery("select * from hotel where hotelName=? ", t);
        if(rst.next()){
            return new Hotel(rst.getInt(1),rst.getString(2),rst.getString(3),
                    rst.getString(4),rst.getString(5),rst.getString(6));
        }else{
            return null;
        }
    }

    @Override
    public ArrayList<Hotel> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<Hotel> traveler=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From hotel");
        while(rst.next()){
            traveler.add(new Hotel(rst.getInt(1),rst.getString(2),rst.getString(3),
                    rst.getString(4),rst.getString(5),rst.getString(6)));
        }
        return traveler;
    }

    @Override
    public ArrayList<Hotel> getAllHotelsForLocation(String Location) throws Exception {
        ArrayList<Hotel>traveler=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From hotel where mainLocationName=? ", Location);
        while(rst.next()){
            traveler.add(new Hotel(rst.getInt(1),rst.getString(2),rst.getString(3),
                    rst.getString(4),rst.getString(5),rst.getString(6)));
        }
        return traveler;
    }
}
