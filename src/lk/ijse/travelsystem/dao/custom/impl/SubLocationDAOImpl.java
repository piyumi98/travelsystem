package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.SubLocationDAO;
import lk.ijse.travelsystem.entity.SubLocation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SubLocationDAOImpl implements SubLocationDAO {
    @Override
    public boolean add(SubLocation subLocation) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into sublocation values (?,? ) ",
                subLocation.getSublocationName(),
                subLocation.getLocationDIstance()
        );
    }

    @Override
    public boolean update(SubLocation subLocation) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("update sublocation set locationDIstance=? where sublocationName=? ",
                subLocation.getLocationDIstance(),
                subLocation.getSublocationName()
        );
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("delete from sublocation where sublocationName=? ", t);
    }

    @Override
    public SubLocation search(String t) throws ClassNotFoundException, SQLException {
        ResultSet rst=CrudUtil.executeQuery("select * from sublocation where sublocationName=? ", t);
        if(rst.next()){
            return new SubLocation(rst.getString(1),rst.getDouble(2));
        }else{
            return null;
        }
    }

    @Override
    public ArrayList<SubLocation> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<SubLocation> locations=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From sublocation");
        while(rst.next()){
            locations.add(new SubLocation(rst.getString(1),rst.getInt(2)));
        }
        return locations;
    }
}
