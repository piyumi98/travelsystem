package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.GuideDAO;
import lk.ijse.travelsystem.entity.Guide;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GuideDAOImpl implements GuideDAO {

    @Override
    public boolean add(Guide guide) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into guide values (null,?,?,?) ",
                guide.getGuideName(),
                guide.getGuideAddress(),
                guide.getGuideTelNo()
        );
    }

    @Override
    public boolean update(Guide guide) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("update guide set guideAddress=?,guideTelNo=? where guideName=? ",
                guide.getGuideAddress(),
                guide.getGuideTelNo(),
                guide.getGuideName()
        );
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("delete from guide where guideName=? ", t);
    }

    @Override
    public Guide search(String t) throws ClassNotFoundException, SQLException {
        ResultSet rst=CrudUtil.executeQuery("select * from guide where guideName=? ", t);
        if(rst.next()){
            return new Guide(rst.getInt(1),rst.getString(2),rst.getString(3),rst.getString(4));
        }else{
            return null;
        }
    }

    @Override
    public ArrayList<Guide> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<Guide>traveler=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From guide");
        while(rst.next()){
            traveler.add(new Guide(rst.getInt(1),rst.getString(2),rst.getString(3),rst.getString(4)));
        }
        return traveler;
    }
}
