package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.PackageDAO;
import lk.ijse.travelsystem.entity.Package;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PackageDAOImpl implements PackageDAO {
    @Override
    public boolean add(Package aPackage) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into package values (?,?,?,?,?,?) ",
                aPackage.getPackageName(),
                aPackage.getGuideID(),
                aPackage.getTourBeginDate(),
                aPackage.getTourEndDate(),
                aPackage.getTourDuration(),
                aPackage.getPackagePrice()
        );
    }

    @Override
    public boolean update(Package aPackage) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("update package set guideID=?,tourBeginDate=?," +
                        "tourEndDate=?,tourDuration=?,packagePrice=? where packageName=?  ",
                aPackage.getGuideID(),
                aPackage.getTourBeginDate(),
                aPackage.getTourEndDate(),
                aPackage.getTourDuration(),
                aPackage.getPackagePrice(),
                aPackage.getPackageName()
        );
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("delete from package where packageName=? ", t);
    }

    @Override
    public Package search(String t) throws ClassNotFoundException, SQLException {
        ResultSet rst=CrudUtil.executeQuery("select * from package where packageName=? ", t);
        if(rst.next()){
            return new Package(rst.getString(1),rst.getInt(2),rst.getString(3),
                    rst.getString(4),rst.getInt(5),rst.getDouble(6));
        }else{
            return null;
        }
    }

    @Override
    public ArrayList<Package> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<Package> traveler=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From package");
        while(rst.next()){
            traveler.add(new Package(rst.getString(1),rst.getInt(2),rst.getString(3),
                    rst.getString(4),rst.getInt(5),rst.getDouble(6)));
        }
        return traveler;
    }
}
