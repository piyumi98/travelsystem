package lk.ijse.travelsystem.dao.custom.impl;



import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.BookingDAO;
import lk.ijse.travelsystem.entity.Booking;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookingDAOImpl implements BookingDAO {
    @Override
    public boolean add(Booking booking) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into booking values (?,?,?,?,?,?) ",
                booking.getBookingID(),
                booking.getTravelID(),
                booking.getPaymentid(),
                booking.getPackageName(),
                booking.getBookingDate(),
                booking.getBookingPersonCount()
        );
    }

    @Override
    public boolean update(Booking booking) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("update booking set paymentid=?,bookingPersonCount=? where bookingID=? ",
                booking.getPaymentid(),
                booking.getBookingPersonCount(),
                booking.getBookingID()
        );
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("delete from booking where bookingID=? ", t);
    }

    @Override
    public Booking search(String t) throws ClassNotFoundException, SQLException {
        return null;
    }

    @Override
    public ArrayList<Booking> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<Booking> bookings=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From booking ");
        while(rst.next()){
            bookings.add(new Booking(rst.getString(1),rst.getString(2),rst.getString(3),rst.getString(4),rst.getString(5),rst.getInt(6)));
        }
        return bookings;
    }
}
