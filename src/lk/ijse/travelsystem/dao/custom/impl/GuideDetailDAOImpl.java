package lk.ijse.travelsystem.dao.custom.impl;


import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.GuideDetailDAO;
import lk.ijse.travelsystem.entity.GuideDetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GuideDetailDAOImpl implements GuideDetailDAO
{
    @Override
    public boolean add(GuideDetails guideDetails) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into guideDetails values (?,?) ",
                guideDetails.getGuideID(),
                guideDetails.getLangName()
        );
    }

    @Override
    public boolean update(GuideDetails guideDetails) throws ClassNotFoundException, SQLException {
        return false;
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return false;
    }

    @Override
    public GuideDetails search(String t) throws ClassNotFoundException, SQLException {
        return null;
    }

    @Override
    public ArrayList<GuideDetails> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<GuideDetails> traveler=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From guideDetails");
        while(rst.next()){
            traveler.add(new GuideDetails(rst.getInt(1),rst.getString(2)));
        }
        return traveler;
    }
}
