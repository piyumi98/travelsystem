package lk.ijse.travelsystem.dao.custom.impl;



import lk.ijse.travelsystem.dao.CrudUtil;
import lk.ijse.travelsystem.dao.custom.HotelDetailDAO;
import lk.ijse.travelsystem.entity.HotelDetail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HotelDetailDAOImpl implements HotelDetailDAO {
    @Override
    public boolean add(HotelDetail hotelDetail) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("insert into hoteldetail values (?,?) ",
                hotelDetail.getHotelID(),
                hotelDetail.getPackageName()
        );
    }

    @Override
    public boolean update(HotelDetail hotelDetail) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("update hoteldetail set packageName=? where hotelID=? ",
                hotelDetail.getPackageName(),
                hotelDetail.getHotelID()
        );
    }

    @Override
    public boolean delete(String t) throws ClassNotFoundException, SQLException {
        return CrudUtil.executeUpdate("delete from hoteldetail where packageName=? ", t);
    }

    @Override
    public HotelDetail search(String t) throws ClassNotFoundException, SQLException {
        return null;
    }

    @Override
    public ArrayList<HotelDetail> getAll() throws ClassNotFoundException, SQLException {
        ArrayList<HotelDetail>traveler=new ArrayList<>();
        ResultSet rst=CrudUtil.executeQuery("select * From hoteldetail");
        while(rst.next()){
            traveler.add(new HotelDetail(rst.getInt(1),rst.getString(2)));
        }
        return traveler;
    }
}
