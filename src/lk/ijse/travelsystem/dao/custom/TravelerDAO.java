package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.Traveler;

public interface TravelerDAO extends SuperDAO<Traveler,String> {
}
