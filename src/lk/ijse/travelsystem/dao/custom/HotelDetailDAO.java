package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.HotelDetail;

public interface HotelDetailDAO extends SuperDAO<HotelDetail,String> {
}
