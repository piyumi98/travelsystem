package lk.ijse.travelsystem.dao.custom;


import lk.ijse.travelsystem.dao.SuperDAO;
import lk.ijse.travelsystem.entity.SubLocation;

public interface SubLocationDAO extends SuperDAO<SubLocation,String> {
}
