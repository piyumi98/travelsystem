package lk.ijse.travelsystem.common;

public class Validation {
    public boolean validateName(String name) {
        boolean nameValidateError= name.matches("[A-Za-z .]+");
        if (!nameValidateError) {
            return false;
        }else {
            return true;
        }
    }

    public boolean validateContactNumber(String contactNumber) {
        if (!contactNumber.matches("[0-9]{10}")) {
            return false;
        }else {
            return true;
        }
    }

    public boolean validateNIC(String NIC) {
        if (!NIC.matches("[0-9]{9}[Vv]{1}")) {
            return false;
        }else {
            return true;
        }

    }
    public boolean onlyNumbers(String number) {
        if (!number.matches("^[0-9]*$")) {
            return false;
        }else {
            return true;
        }

    }
    public boolean email(String NIC) {
        if (!NIC.matches("([A-Za-z0-9-_.]+@[A-Za-z0-9-_]+(?:\\.[A-Za-z0-9]+)+)")) {
           return false;
        }else {
         return true;
        }
    }
public boolean date(String NIC) {
        if (!NIC.matches("^\\d{4}-\\d{2}-\\d{2}$")) {
           return false;
        }else {
         return true;
        }
    }
    public boolean address(String NIC) {
        if (!NIC.matches("\\d+\\s+([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)")) {
           return false;
        }else {
         return true;
        }
    }

}
