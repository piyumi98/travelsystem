package lk.ijse.travelsystem.common;



import lk.ijse.travelsystem.db.DBConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class IDController {

    public static String getLastID(String tblName,String coloumnName) throws Exception{
        String sql="SELECT " +coloumnName+ " FROM " +tblName+ " order by 1 desc limit 1";
        Connection connection= DBConnection.getInstance().getConnection();
        Statement statement=connection.createStatement();
        ResultSet set=statement.executeQuery(sql);
        if (set.next()){
            return set.getString(1);
        }
        return null;
    }
}
